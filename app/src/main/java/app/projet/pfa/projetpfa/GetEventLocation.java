package app.projet.pfa.projetpfa;

public class GetEventLocation {

    private String id;
    private String lat;
    private String lang;


    public GetEventLocation(){}

    public GetEventLocation(String id, String lat, String lang) {
        this.id = id;
        this.lat = lat;
        this.lang = lang;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
