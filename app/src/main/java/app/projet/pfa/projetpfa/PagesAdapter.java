package app.projet.pfa.projetpfa;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Toast;

/**
 * Created by koussai on 22/03/2018.
 */

class PagesAdapter extends FragmentPagerAdapter{
    public PagesAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                RequeteFragment requeteFragment = new RequeteFragment();
                return requeteFragment;

            case 1:
                FriendFragment friendFragment = new FriendFragment();
                return friendFragment;

            case 2:
                ChatFragment chatFragment = new ChatFragment();
                return chatFragment;

            default:
                return null;

        }

    }



    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:

                return "Les evenements";

            case 1:
                return "Amis";

            case 2:
                return "Conversation";

            default:
                return null;
        }
    }
}
