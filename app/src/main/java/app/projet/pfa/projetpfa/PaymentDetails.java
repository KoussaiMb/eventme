package app.projet.pfa.projetpfa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentDetails extends AppCompatActivity {

    TextView textViewID,textViewSt,textViewAm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        textViewID = (TextView)findViewById(R.id.textID);
        textViewSt = (TextView)findViewById(R.id.textStatut);
        textViewAm = (TextView)findViewById(R.id.textAmount);

        Intent intent = getIntent();

        try{
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonObject.getJSONObject("response"),intent.getStringExtra("PaymentAmount"));

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    private void showDetails(JSONObject response, String paymentAmount) {
        try{
            textViewID.setText(response.getString("id"));
            textViewSt.setText(response.getString("state"));
            textViewAm.setText(response.getString(paymentAmount + "euros"));

        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
