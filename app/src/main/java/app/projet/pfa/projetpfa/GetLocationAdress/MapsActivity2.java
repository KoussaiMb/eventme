package app.projet.pfa.projetpfa.GetLocationAdress;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import app.projet.pfa.projetpfa.R;

public class MapsActivity2 extends AppCompatActivity
        implements OnStreetViewPanoramaReadyCallback {

    private static final LatLng NY_TIME_SQUARE = new LatLng(40.758952, -73.985174); // sample input.
    private StreetViewPanorama mStreetViewPanorama; // the object that handle panorama preview.
    private static double latD, langD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps3);
        SupportStreetViewPanoramaFragment panoramaFragment =
                (SupportStreetViewPanoramaFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        panoramaFragment.getStreetViewPanoramaAsync(this); // call this to use the fragment.

        String lati = getDefaults("latData",this);
        String langi = getDefaults("langData",this);

        latD = Double.parseDouble(lati);
        langD = Double.parseDouble(langi);

        Log.d("ayalat",latD + " " + langD);



    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    /**
     * after panorama get ready, you can declare the position.
     */
    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        mStreetViewPanorama = streetViewPanorama;
        LatLng latLng = new LatLng(latD,langD);
        mStreetViewPanorama.setPosition(latLng); // where the street view will be shown.
        /** you can control the inputs into street view */
        mStreetViewPanorama.setUserNavigationEnabled(false);
        mStreetViewPanorama.setPanningGesturesEnabled(true);
        mStreetViewPanorama.setZoomGesturesEnabled(true);
    }
}