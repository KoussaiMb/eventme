package app.projet.pfa.projetpfa;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplachScreen extends AppCompatActivity {
    private static int SLPASH = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splach_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent inetent = new Intent(SplachScreen.this, DashbordActivity.class);
                startActivity(inetent);
                finish();
            }
        },SLPASH);
    }
}
