package app.projet.pfa.projetpfa;

public class GetIdOfEvenet {
    private String id;

    public GetIdOfEvenet() {
    }

    public GetIdOfEvenet(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
