package app.projet.pfa.projetpfa;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static app.projet.pfa.projetpfa.DashbordActivity.option;


public class EvenementOptionFragment extends Fragment {

    private View mainView;
    private static String getInetentFormIndex = "";
    private RecyclerView recyclerViewFriend;
    private DatabaseReference databaseReference, databaseReferenceUsers,databaseReferenceKey;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReferenceEvenet;
    private String current_id_user;



    public EvenementOptionFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //Toast.makeText(getActivity(),"c'est moi la famille" + option, Toast.LENGTH_LONG).show();


        mainView = inflater.inflate(R.layout.fragment_evenement_option, container, false);


        recyclerViewFriend = (RecyclerView) mainView.findViewById(R.id.listEvenetOptionFragment);
        firebaseAuth = FirebaseAuth.getInstance();
        current_id_user = firebaseAuth.getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceKey = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReference.keepSynced(true);
        databaseReferenceKey.keepSynced(true);
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReferenceUsers.keepSynced(true);
        recyclerViewFriend.setHasFixedSize(true);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(getContext()));




        return mainView;
    }


    @Override
    public void onStart() {
        super.onStart();

        Query query = databaseReference.orderByChild("categorie").equalTo(option);

        FirebaseRecyclerAdapter<Evenement, EvenementOptionFragment.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, EvenementOptionFragment.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                EvenementOptionFragment.UsersHolder.class,
                query
        ) {
            @Override
            protected void populateViewHolder(final EvenementOptionFragment.UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());

                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, getActivity().getApplicationContext());


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, getActivity().getApplicationContext());


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, getActivity().getApplicationContext());


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, getActivity().getApplicationContext());


                }


                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(getActivity(), viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        recyclerViewFriend.setAdapter(firebaseRecyclerAdapter);
    }

    public static class UsersHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setLieu(String date) {

            TextView textView = (TextView) mView.findViewById(R.id.textView3);
            textView.setText(date);
        }

        public void setTitle(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.textView2);
            textView.setText(name);
        }

        public void setDate(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.textView4);
            textView.setText(name);
        }

        public void setUserImage(int thumb_image, Context ctx) {

            ImageView userImageView = (ImageView) mView.findViewById(R.id.imageView4);
            userImageView.setImageResource(thumb_image);


        }



    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
//        getMenuInflater().inflate(R.menu.menu_serach_users, menu);
//
//        MenuItem item = menu.findItem(R.id.search);
//        materialSearchView.setMenuItem(item);
//
//        return true;
//    }


}

