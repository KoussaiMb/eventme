package app.projet.pfa.projetpfa;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.http.RequestHandle;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.RequestHandler;

import org.w3c.dom.Text;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class InscritActivity extends AppCompatActivity implements  View.OnClickListener{

    private AutoCompleteTextView chEmail,psedeo;
    private EditText chPass;
    private Button btnInsc,btnFb,btnGoogle;
    private ProgressDialog progressDialog;
    private FirebaseAuth auth;
    static  String displayName;
    public static String passingEmailuser;
    public static String device_token;
    private EditText editTextPays, editTextPhone, editText;
    private TextView textViewDateNaissance;
    private RadioGroup radioGroup;
    private RadioButton radioButtonH,radioButtonF;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String dateNaissance = "";
    String ServerURL = "http://192.168.1.100/pfa/get_data.php" ;
    String sexe = "";
    private ProgressDialog mProgressDialog;
    private Spinner country;
    String nameWeb, emailWeb;
    String ageUser = "";
    JSONParser jsonParser = new JSONParser();


    // url to create new product
    private static String url_create_product = "jdbc:mysql//192.168.43.143/pfa";
    private static String USER ="root";
    private static String PASS ="";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";



    private DatabaseReference firebaseDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        chEmail = (AutoCompleteTextView) findViewById(R.id.email);
        psedeo = (AutoCompleteTextView) findViewById(R.id.psedeo);
        chPass = (EditText) findViewById(R.id.create_new_password);
//        editTextPays = (EditText) findViewById(R.id.pays);
        editTextPhone = (EditText) findViewById(R.id.Phone);
        country = (Spinner) findViewById(R.id.pays);
        textViewDateNaissance = (TextView)findViewById(R.id.dateN);
        radioGroup = (RadioGroup)findViewById(R.id.sexeRg);


        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);
        for (String country : countries) {
            System.out.println(country);
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        // set the view for the Drop down list
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set the ArrayAdapter to the spinner
        country.setAdapter(dataAdapter);
        country.setSelection(37);

        System.out.println("# countries found: " + countries.size());




        btnInsc = (Button) findViewById(R.id.btnInsc);
       // btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnInsc.setOnClickListener(this);

        textViewDateNaissance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                String test="Date de début ";

                DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dateNaissance =""+dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                                Calendar now = Calendar.getInstance();   // Gets the current date and time
                                int anne = now.get(Calendar.YEAR);

                                int getNAi = anne -year;
                                String aggge = String.valueOf(getNAi);
                                textViewDateNaissance.setText(aggge +" ans");


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        progressDialog = new ProgressDialog(this);

        auth = FirebaseAuth.getInstance();

    }

    private void registerUser() {
        final String email = chEmail.getText().toString().trim();
        final String password = chPass.getText().toString().trim();
        final String nom = psedeo.getText().toString().trim();
        final String countryNameSelected = country.getSelectedItem().toString();
        final String cId = String.valueOf(country.getSelectedItemId());
        final String phone = editTextPhone.getText().toString().trim();

        int selectedRadioButtonID = radioGroup.getCheckedRadioButtonId();

        if(selectedRadioButtonID == -1)
        {
            Toast.makeText(InscritActivity.this,"champs obligatoire", Toast.LENGTH_LONG).show();
            return;

        }
        else {
            RadioButton selectedRadioButton = (RadioButton) findViewById(selectedRadioButtonID);
            sexe = selectedRadioButton.getText().toString();
        }







        if (dateNaissance.isEmpty()) {
            editTextPays.setError("champs date de naissance est obligatoire");
            editTextPays.requestFocus();
            return;
        }

//        if (pays.isEmpty()) {
//            editTextPays.setError("champs pays est obligatoire");
//            editTextPays.requestFocus();
//            return;
//        }

        if (phone.isEmpty()) {
            editTextPhone.setError("champs phone est obligatoire");
            editTextPhone.requestFocus();
            return;
        }

        if (phone.length() != 8 ) {
            chPass.setError("Min 8 carcateres ou nombres");
            chPass.requestFocus();
            return;
        }


        if (email.isEmpty()) {
            chEmail.setError("champs email est obligatoire");
            chEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            chEmail.setError("SVP entrez un email valide");
            chEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            chPass.setError("Mot de passe est obligatoire ");
            chPass.requestFocus();
            return;
        }

        if (password.length() < 6) {
            chPass.setError("Min 6 carcateres ou nombres");
            chPass.requestFocus();
            return;
        }

        if(nom.isEmpty()){
            psedeo.setError("champs nom est obligatoire");
            chEmail.requestFocus();
            return;
        }

        //progressBar.setVisibility(View.VISIBLE);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Chargeemnt des données");
        mProgressDialog.setMessage("Inscription  en cours ");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()) {
                    passingEmailuser =email;

                    GetData();
                    InsertData(nom, email, countryNameSelected, sexe, phone);
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                  //  new CreateNewProduct().execute();

                    String uid = user.getUid();

                    firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                    device_token = FirebaseInstanceId.getInstance().getToken();

                    HashMap<String, String> hashMapUser = new HashMap<>();
                    hashMapUser.put("email",email);
                    //passingEmail=email.toString();
                    hashMapUser.put("nom",nom);
                    hashMapUser.put("sexe",sexe);
                    hashMapUser.put("age",ageUser);
                    hashMapUser.put("pays",countryNameSelected);
                    hashMapUser.put("phone",phone);
                    hashMapUser.put("ville","ville");
                    hashMapUser.put("image","default");
                    hashMapUser.put("thumb_image","default");
                    hashMapUser.put("device_token",device_token);



                    Users users = new Users(nom,dateNaissance,sexe,cId, countryNameSelected, "default","default", phone, email, device_token);

//                    firebaseDatabase.setValue(hashMapUser).addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            if(task.isSuccessful())
//                            {
//                              Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
//                              Intent intent = new Intent(InscritActivity.this, DashbordActivity.class);
//                              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                              startActivity(intent);
//                              finish();
//                            }
//                        }
//                    });



                    firebaseDatabase.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                            {

                                Thread thread = new Thread(){
                                    @Override
                                    public void run() {
                                        try {
                                            synchronized (this) {
                                                wait(5000);

                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
//                                                        Send send = new Send();
//                                                        send.execute();
                                                        Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
                                                        mProgressDialog.dismiss();
                                                        Intent intent = new Intent(InscritActivity.this, DashbordActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();

//
                                                    }
                                                });

                                            }
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }

                                    };
                                };
                                thread.start();


                            }
                        }
                    });





                } else {
                    mProgressDialog.dismiss();

                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(getApplicationContext(), "utilisateur deja inscrit", Toast.LENGTH_LONG).show();

                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInsc:
                registerUser();
                break;


        }
    }

    /**
     * Background Async Task to Create new product
     * */
//    class Send extends AsyncTask<Void, Void, String> {
//
//
//
//        String msg ="";
//        final String email = chEmail.getText().toString().trim();
//        final String password = chPass.getText().toString().trim();
//        final String nom = psedeo.getText().toString().trim();
//        final String pays = editTextPays.getText().toString().trim();
//        final String phone = editTextPhone.getText().toString().trim();
//
//
//        @Override
//        protected String doInBackground(Void... voids) {
//
//            HashMap<String,String> params = new HashMap<>();
//            params.put(ConfigPhp.email, email);
//
////            RequestHandlers rh = new RequestHandlers();
////            String res = rh.sendPostRequest(ConfigPhp.USER_ADD);
//
//
//            return res;
//        }
//
//        /**
//         * Before starting background thread Show Progress Dialog
//         * */
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//        }
//
//        /**
//         * Creating product
//         * */
//
//
//        /**
//         * After completing background task Dismiss the progress dialog
//         * **/
//        protected void onPostExecute(String file_url) {
//            // dismiss the dialog once done
//            Log.i("msg", file_url);
//        }
//
//    }

    public void GetData(){

        nameWeb = chEmail.getText().toString();

        emailWeb = psedeo.getText().toString();

    }

    public void InsertData(final String name, final String email, final String pays, final String sexe, final String phone){

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {

                String NameHolder = name ;
                String EmailHolder = email ;
                String PaysHolder = pays ;
                String SexeHolder = sexe ;
                String PhoneHolder = phone ;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("name", NameHolder));
                nameValuePairs.add(new BasicNameValuePair("email", EmailHolder));
                nameValuePairs.add(new BasicNameValuePair("pays", PaysHolder));
                nameValuePairs.add(new BasicNameValuePair("sexe", SexeHolder));
                nameValuePairs.add(new BasicNameValuePair("phone", PhoneHolder));

                try {
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(ServerURL);

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse httpResponse = httpClient.execute(httpPost);

                    HttpEntity httpEntity = httpResponse.getEntity();


                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
                return "Data Inserted Successfully";
            }

            @Override
            protected void onPostExecute(String result) {

                super.onPostExecute(result);

//                Toast.makeText(InscritActivity.this, "Data Submit Successfully", Toast.LENGTH_LONG).show();

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();

        sendPostReqAsyncTask.execute(name, email, pays, sexe, phone);
    }

    private String getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }
}
