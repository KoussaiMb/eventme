package app.projet.pfa.projetpfa;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import static java.lang.Math.toIntExact;


import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SlideActivity extends AppCompatActivity {

    ViewPager viewPager;
    LinearLayout linearLayout;

    private TextView[] mDots;
    private SlideAdapter activity_slide;

    private Button buttonN;
    private Button buttonB;

    private int nCurrentPage;

    String getIntentKeyOfEvent;


    DatabaseReference databaseReference;

    ArrayList<String> arrayList = new ArrayList<String>();
    static  long count;


    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    private DatabaseReference mUsersDatabase;

    private LinearLayoutManager mLayoutManager;

    MaterialSearchView materialSearchView;

    static  String passingS;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);


        getIntentKeyOfEvent = getIntent().getStringExtra("eventKey");

        Toast.makeText(SlideActivity.this, "slide id" + getIntentKeyOfEvent, Toast.LENGTH_LONG).show();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("IamgeEvent").child(getIntentKeyOfEvent);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                count = dataSnapshot.getChildrenCount();

                Toast.makeText(SlideActivity.this, "count = " + count, Toast.LENGTH_LONG).show();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    String s = postSnapshot.getKey().toString();


                    String ss = dataSnapshot.child(s).child("ivalue").getValue().toString();

                    passingS = s;


                    arrayList.add(ss);

                    for (String x : arrayList) {
                        Log.i("xxxx", x);
                    }


                    arrayList.add(ss);


                    Log.i("idSS", ss);


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        StorageReference mStorageRef;

        mStorageRef = FirebaseStorage.getInstance().getReference();

        mStorageRef.child(getIntentKeyOfEvent).getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {


            }
        });


//        Toast.makeText(SlideActivity.this,""+ getIntentKeyOfEvent, Toast.LENGTH_LONG).show();

//
//        viewPager = (ViewPager)findViewById(R.id.vp);
//        linearLayout = (LinearLayout)findViewById(R.id.lly);
//
//        buttonN = (Button)findViewById(R.id.btnN);
//        buttonB = (Button)findViewById(R.id.btnB);
//
//
//        activity_slide = new SlideAdapter(this);
//
//        viewPager.setAdapter(activity_slide);
//
//        addDotsIndicator(0);
//        viewPager.addOnPageChangeListener(viewListner);
//
//        //onClickListener
//
//        buttonN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                viewPager.setCurrentItem(nCurrentPage +1);
//            }
//        });
//
//        buttonB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                viewPager.setCurrentItem(nCurrentPage -1);
//            }
//        });
//
//
//        int x = getData();
//
//        Toast.makeText(SlideActivity.this,"" +x , Toast.LENGTH_LONG).show();
//
//
//
//    }
//
//
//    private int getData(){
//
//        int x = arrayList.size();
//        for(String a : arrayList){
//            Log.i("myDataArrayList", a);
//        }
//        return  x;
//    }
//
//    public void addDotsIndicator(int position){
//
//        mDots = new TextView[3];
//        linearLayout.removeAllViews();
//
//        for (int i = 0; i <mDots.length; i++ ){
//
//            mDots[i] = new TextView(this);
//            mDots[i].setText(Html.fromHtml("&#8226"));
//            mDots[i].setTextSize(35);
//            mDots[i].setTextColor(getResources().getColor(R.color.grey));
//
//            linearLayout.addView(mDots[i]);
//        }
//
//        if(mDots.length > 0){
//
//            mDots[position].setTextColor(getResources().getColor(R.color.white));
//        }
//    }
//
//    ViewPager.OnPageChangeListener viewListner = new ViewPager.OnPageChangeListener() {
//        @Override
//        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//        }
//
//        @Override
//        public void onPageSelected(int i) {
//
//            addDotsIndicator(i);
//
//            nCurrentPage = i;
//
//            if (i==0){
//                buttonN.setEnabled(true);
//                buttonB.setEnabled(false);
//                buttonB.setVisibility(View.INVISIBLE);
//
//                buttonN.setText("Next");
//                buttonB.setText("");
//            }else if (i==mDots.length-1) {
//                buttonN.setEnabled(true);
//                buttonB.setEnabled(true);
//                buttonB.setVisibility(View.VISIBLE);
//
//                buttonN.setText("Finish");
//                buttonB.setText("Back");
//
//            } else {
//                buttonN.setEnabled(true);
//                buttonB.setEnabled(true);
//                buttonB.setVisibility(View.VISIBLE);
//
//                buttonN.setText("Next");
//                buttonB.setText("BACK");
//            }
//
//        }
//
//        @Override
//        public void onPageScrollStateChanged(int state) {
//
//        }
//    };


        mLayoutManager = new LinearLayoutManager(this);

        mUsersList = (RecyclerView) findViewById(R.id.rcvSlide);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(mLayoutManager);


    }

    @Override
    protected void onStart() {
        super.onStart();

        for(int i=0;i<arrayList.size();i++){
            databaseReference.child(passingS).child(arrayList.get(i));
            Log.i("getData",databaseReference.child(passingS).child(arrayList.get(i)).toString());
        }


        FirebaseRecyclerAdapter<MultipleImage.MultiImage, SlideActivity.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<MultipleImage.MultiImage, SlideActivity.UsersViewHolder>(

                MultipleImage.MultiImage.class,
                R.layout.test_layout_image_slide_event,
                SlideActivity.UsersViewHolder.class,
                databaseReference


        ) {
            @Override
            protected void populateViewHolder(SlideActivity.UsersViewHolder usersViewHolder, MultipleImage.MultiImage users, int position) {

                usersViewHolder.setUserImage(users.getIvalue(), getApplicationContext());

                final String user_id = getRef(position).getKey();


                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                    }
                });

            }
        };


        mUsersList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }



        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.event_image_slide_test_layout);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user2).into(userImageView);

        }


    }

}
