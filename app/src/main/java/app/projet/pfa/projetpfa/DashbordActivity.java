 package app.projet.pfa.projetpfa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.hitomi.cmlibrary.OnMenuStatusChangeListener;

import static app.projet.pfa.projetpfa.EventActivity.tes;

public class DashbordActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener{

    private FirebaseAuth mAuth;
    CircleMenu circleMenu;
    private Switch aSwitch;
    private ScrollView scrollView, scrollView1;
    private ImageView imageViewM, imageViewR, imageViewS, imageViewA;

    private DatabaseReference databaseReference, reference;
    String musique = "musique";
    String autre = "autre";
    String rencontre = "renocntre";
    String sport = "sport";
    protected static String option ="musique";
    GoogleSignInClient mGoogleSignInClient;
    public GoogleApiClient googleApiClient;
    static String google="";






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashbord);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       // checkConnection();

        aSwitch = (Switch)findViewById(R.id.switchId);
        scrollView = (ScrollView)findViewById(R.id.scv);
        scrollView1 = (ScrollView)findViewById(R.id.scv1);

        circleMenu = (CircleMenu) findViewById(R.id.circle_menu);

        imageViewM = (ImageView)findViewById(R.id.musqiue);
        imageViewR = (ImageView)findViewById(R.id.rencontre);
        imageViewS = (ImageView)findViewById(R.id.sport);
        imageViewA = (ImageView)findViewById(R.id.autre);

        imageViewA.setOnClickListener(this);
        imageViewS.setOnClickListener(this);
        imageViewR.setOnClickListener(this);
        imageViewM.setOnClickListener(this);



       if(getIntent().getExtras() != null){
           google = "oui";
       }else {
           google = "";
       }


//       Toast.makeText(DashbordActivity.this, google, Toast.LENGTH_LONG).show();



        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    circleMenu.setVisibility(View.VISIBLE);
                    a();
                    scrollView.setVisibility(View.GONE);
                    scrollView1.setVisibility(View.GONE);
                }



                else{

                    circleMenu.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    scrollView1.setVisibility(View.VISIBLE);

                }


            }
        });




        mAuth = FirebaseAuth.getInstance();
//        if(mAuth.getLanguageCode().toString().isEmpty())
//        {
//            Log.i("codeLangPays",  mAuth.getLanguageCode().toString());
//        }


        if(mAuth.getCurrentUser() != null){

            databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
        }
        reference = FirebaseDatabase.getInstance().getReference().child("Users");



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashbordActivity.this, TestMenuCircle.class));
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        fab.setVisibility(View.GONE);

//      DatabaseReference  databaseReferenceKey = FirebaseDatabase.getInstance().getReference().child("Evenement");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

//    private void checkConnection() {
//        boolean isConnected = ConnectivityReceiver.isConnected();
//    }

//    private void showSnack(boolean isConnected) {
//        String message;
//        int color;
//        if (isConnected) {
//            message = "Good! Connected to Internet";
//            color = Color.WHITE;
//        } else {
//            message = "Sorry! Not connected to internet";
//            color = Color.RED;
//        }
//
//        Snackbar snackbar = Snackbar
//                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);
//
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(color);
//        snackbar.show();
//    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
       // MyApplication.getInstance().setConnectivityListener(this);
    }

    private void a() {
        circleMenu.setMainMenu(Color.parseColor("#CDCDCD"), R.mipmap.icon_menu, R.mipmap.icon_cancel);
        circleMenu.addSubMenu(Color.parseColor("#258CFF"), R.drawable.ic_audiotrack_white_24dp)
                .addSubMenu(Color.parseColor("#FF4B32"), R.drawable.ic_directions_bike_white_24dp)
                .addSubMenu(Color.parseColor("#8A39FF"), R.drawable.ic_wc_white_24dp)
                .addSubMenu(Color.parseColor("#FF6A00"), R.drawable.ic_format_align_left_white_24dp);

//

        circleMenu.setOnMenuSelectedListener(new OnMenuSelectedListener() {

                                                 @Override
                                                 public void onMenuSelected(int index) {
                                                     switch (index) {
                                                         case 0:

                                                             option = "Musique";
                                                             Intent intent = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
                                                             // intent.putExtra("getImageOption", rencontre);
                                                             startActivity(intent);
                                                             break;

                                                         case 1:

                                                             option = "Sport";
//                Intent intent3 = new Intent(DashbordActivity.this, MultipleImage.class);
                                                             Intent intent3 = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
//                  Intent intent3 = new Intent(DashbordActivity.this, app.projet.pfa.projetpfa.forum.MainActivity.class);

                                                             // intent.putExtra("getImageOption", rencontre);
                                                             startActivity(intent3);
                                                             break;

                                                         case 2:

                                                             option = "Rencontre";
                                                             Intent intent1 = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
                                                             // intent.putExtra("getImageOption", rencontre);
                                                             startActivity(intent1);
                                                             break;

                                                         case 3:

                                                             option = "Autre";
                                                             Intent intent2 = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
                                                             // intent.putExtra("getImageOption", rencontre);
                                                             startActivity(intent2);
                                                             break;



                                                     }
                                                 }
                                             }

        );

        circleMenu.setOnMenuStatusChangeListener(new OnMenuStatusChangeListener() {

                                                     @Override
                                                     public void onMenuOpened() {

                                                     }

                                                     @Override
                                                     public void onMenuClosed() {

                                                     }
                                                 }
        );
    }






    private void deleteUser(String uid){
        reference.child(uid).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (circleMenu.isOpened())
            circleMenu.closeMenu();
        else
            finish();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashbord, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            suppUser();

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(DashbordActivity.this);
            builder.setMessage("Vous voulez vraiment supprimer ?").setPositiveButton("Oui", dialogClickListener)
                    .setNegativeButton("Non", dialogClickListener).show();
        }

        if(id == R.id.Profile){
            //change style
            startActivity(new Intent(DashbordActivity.this, ProfileActivity.class));

        }

        if (id == R.id.desconnexion){

            databaseReference.child("online").setValue(ServerValue.TIMESTAMP);

//            if(google.equals("oui")){
//                signOutG();
//
//            }else {
                FirebaseAuth.getInstance().signOut();

//            }

            sendToMain();


        }

        return super.onOptionsItemSelected(item);
    }

    private void suppUser() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String getUid = user.getUid();
        if (user != null) {

            user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if(task.isSuccessful()){


                        deleteUser(getUid);
                        Toast.makeText(DashbordActivity.this,"supprimer ", Toast.LENGTH_LONG).show();
                        sendToMain();

                    }else{
                        Toast.makeText(DashbordActivity.this,"errure " + task.getException(), Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){

            sendToMain();

        }
        else {
            databaseReference.child("online").setValue("true");
        }

    }


    @Override
    protected void onStop() {
        super.onStop();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser != null) {

            databaseReference.child("online").setValue(ServerValue.TIMESTAMP);

        }
    }

    private void sendToMain(){

        Intent intent  = new Intent(DashbordActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.fragementPage){
            Intent intent = new Intent(this.getApplicationContext(),PrincipaleActivity.class);
            startActivity(intent);


        }

        if (id == R.id.nav_camera) {

            startActivity(new Intent(this.getApplicationContext(),usersActivity.class));

        } else if (id == R.id.even) {
           startActivity(new Intent(this.getApplicationContext(),EventActivity.class));

        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(this.getApplicationContext(),ChatBoot.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {
                       startActivity(new Intent(this.getApplicationContext(),PubActivity.class));


        } else if (id == R.id.nav_send) {
            databaseReference.child("online").setValue(ServerValue.TIMESTAMP);

//            if(google.equals("oui")){
//                signOutG();
//
//            }else {
            FirebaseAuth.getInstance().signOut();

//            }

            sendToMain();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void passindDataToFragment(String data){

        Bundle bundle = new Bundle();
        bundle.putString("pass", data);
        EvenementOptionFragment evenementOptionFragment = new EvenementOptionFragment();
        evenementOptionFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,evenementOptionFragment).commit();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.musqiue:
                option = "Musique";
                Intent intent = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
               // intent.putExtra("getImageOption", rencontre);
                startActivity(intent);
                break;

            case R.id.rencontre:
                option = "Rencontre";
                Intent intent1 = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
                // intent.putExtra("getImageOption", rencontre);
                startActivity(intent1);
                break;

            case R.id.autre:
                option = "Autre";
                Intent intent2 = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
                // intent.putExtra("getImageOption", rencontre);
                startActivity(intent2);
                break;

            case R.id.sport:
                option = "Sport";
//                Intent intent3 = new Intent(DashbordActivity.this, MultipleImage.class);
                Intent intent3 = new Intent(DashbordActivity.this, DisplayEventOptionActivity.class);
//                  Intent intent3 = new Intent(DashbordActivity.this, app.projet.pfa.projetpfa.forum.MainActivity.class);

                // intent.putExtra("getImageOption", rencontre);
                startActivity(intent3);
                break;

            default:
                break;

        }



    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
       // showSnack(isConnected);
    }

//    public void signOutG() {
//        // Firebase sign out
//        mAuth.signOut();
//
//        // Google sign out
//        Auth.GoogleSignInApi.signOut(googleApiClient);
//    }
}
