package app.projet.pfa.projetpfa;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MultipleImage extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 1;
    String getEvenetKey="";
    private ImageButton mSelectBtn;
    private CircleImageView circleImageView;
    private RecyclerView mUploadList;

    private List<String> fileNameList;
    private List<String> fileDoneList;
    private ArrayList<String> list = new ArrayList<String>();

    private UploadListAdapter uploadListAdapter;

    private StorageReference mStorage,mStorageThumb;
    private DatabaseReference databaseReferenceImageEvent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_image);

        getEvenetKey = getIntent().getStringExtra("evenetKey");

       // Toast.makeText(MultipleImage.this, "" + getEvenetKey, Toast.LENGTH_LONG).show();

            mStorage = FirebaseStorage.getInstance().getReference();
            mStorageThumb = FirebaseStorage.getInstance().getReference();

        mSelectBtn = (ImageButton) findViewById(R.id.select_btn);
        mUploadList = (RecyclerView) findViewById(R.id.upload_list);


        fileNameList = new ArrayList<>();
        fileDoneList = new ArrayList<>();

        uploadListAdapter = new UploadListAdapter(fileNameList, fileDoneList);

        //RecyclerView

        mUploadList.setLayoutManager(new LinearLayoutManager(this));
        mUploadList.setHasFixedSize(true);
        mUploadList.setAdapter(uploadListAdapter);

        databaseReferenceImageEvent = FirebaseDatabase.getInstance().getReference().child("IamgeEvent").child(getEvenetKey);


        mSelectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), RESULT_LOAD_IMAGE);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK){

            if(data.getClipData() != null){

                int totalItemsSelected = data.getClipData().getItemCount();

                for(int i = 0; i < totalItemsSelected; i++){

                    final Uri fileUri = data.getClipData().getItemAt(i).getUri();

                    final String fileName = getFileName(fileUri);

                    Log.i("fileName1", fileName);


                    fileNameList.add(fileName);
                    fileDoneList.add("uploading");
                    uploadListAdapter.notifyDataSetChanged();

                    StorageReference fileToUpload = mStorage.child("Images").child(getEvenetKey).child(fileName);

                    final int finalI = i;
                    fileToUpload.putFile(fileUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                            if(task.isSuccessful()){


                                 String donwalod_uri = task.getResult().getDownloadUrl().toString();

                                 Log.i("myDow", donwalod_uri);

                                list.add(donwalod_uri);

                                for (String e : list){
                                    Log.i("listDow", donwalod_uri);
                                }


//                                for(String t  : list){

                                    final MultiImage multiImage = new MultiImage();
                                    multiImage.setIvalue(donwalod_uri);
//                                }

                                 databaseReferenceImageEvent.push().setValue(multiImage).addOnCompleteListener(new OnCompleteListener<Void>() {
                                     @Override
                                     public void onComplete(@NonNull Task<Void> task) {

                                         Log.i("nbre", multiImage.toString());
                                         if(!task.isSuccessful())
                                         {
                                             Toast.makeText(MultipleImage.this,""+ task.getException(), Toast.LENGTH_LONG).show();
                                         }
                                     }
                                 });

                                 fileDoneList.remove(finalI);
                                 fileDoneList.add(finalI, "done");

                            uploadListAdapter.notifyDataSetChanged();

                            startActivity(new Intent(MultipleImage.this, DashbordActivity.class));
//




                            }

                        }
                    });
//                     fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess(UploadTask.TaskSnapshot task) {
//
//
//
//                                    fileDoneList.remove(finalI);
//                            fileDoneList.add(finalI, "done");
//
//                            uploadListAdapter.notifyDataSetChanged();
//
//
//
////
////                            databaseReferenceImageEvent.setValue(list).addOnCompleteListener(new OnCompleteListener<Void>() {
////                                @Override
////                                public void onComplete(@NonNull Task<Void> task) {
////                                    if(task.isSuccessful()){
////
////
////
////                                        Toast.makeText(MultipleImage.this, "Success Uploading.", Toast.LENGTH_LONG).show();
////
////                                    }
////                                }
////                            });
//
//                        }
//                    });

                }

                //Toast.makeText(MainActivity.this, "Selected Multiple Files", Toast.LENGTH_SHORT).show();

            } else if (data.getData() != null){

                Toast.makeText(MultipleImage.this, "Selected Single File", Toast.LENGTH_SHORT).show();

            }

        }

    }



    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public static class MultiImage{
        private String Ikey;
        private String Ivalue;


        public MultiImage() {}

        public MultiImage(String Ivalue){
            Ivalue = Ivalue;
        }


        public String getIvalue() {
            return Ivalue;
        }

        public void setIvalue(String ivalue) {
            Ivalue = ivalue;
        }

        @Override
        public String toString() {
            return this.getIvalue() ;
        }
    }



}












