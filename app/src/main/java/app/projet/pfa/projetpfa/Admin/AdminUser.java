package app.projet.pfa.projetpfa.Admin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.picasso.Picasso;

import app.projet.pfa.projetpfa.ChatActivity;
import app.projet.pfa.projetpfa.DashbordActivity;
import app.projet.pfa.projetpfa.ProfileActivity;
import app.projet.pfa.projetpfa.R;
import app.projet.pfa.projetpfa.Users;
import app.projet.pfa.projetpfa.userProfilVisiteActivity;
import app.projet.pfa.projetpfa.usersActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdminUser extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    private DatabaseReference mUsersDatabase;

    private LinearLayoutManager mLayoutManager;

    MaterialSearchView materialSearchView;

    ImageView  imageView;

    FloatingActionButton floatingActionButton;

    private DatabaseReference databaseReference, reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_user);

        mToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(mToolbar);


        getSupportActionBar().setTitle("Retrouvez des amis");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        materialSearchView = (MaterialSearchView)findViewById(R.id.mysearch);
        materialSearchView.clearFocus();

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        reference = FirebaseDatabase.getInstance().getReference().child("Users");


        mLayoutManager = new LinearLayoutManager(this);

        mUsersList = (RecyclerView) findViewById(R.id.rcv);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(mLayoutManager);

        floatingActionButton = (FloatingActionButton)findViewById(R.id.floatingActionButton2);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminUser.this, AddUserAdmin.class));
            }
        });

        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // firebaseUserSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                firebaseUserSearch(newText);
                return false;
            }
        });



    }

    private void firebaseUserSearch(String searchText) {


        Query firebaseSearchQuery = mUsersDatabase.orderByChild("nom").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<Users, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(

                Users.class,
                R.layout.list_user_by_admin,
                AdminUser.UsersViewHolder.class,
                mUsersDatabase

        ) {
            @Override
            protected void populateViewHolder(AdminUser.UsersViewHolder usersViewHolder, Users users, int position) {

                usersViewHolder.setDisplayName(users.getNom());
                usersViewHolder.setUserStatus(users.getPays());
                usersViewHolder.setUserImage(users.getThumb_image(), getApplicationContext());

                final String user_id = getRef(position).getKey();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                final String curenUserUid = user.getUid();

                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                            Intent profileIntent = new Intent(AdminUser.this, userProfilVisiteActivity.class);
                            profileIntent.putExtra("user_id", user_id);
                            startActivity(profileIntent);




                    }
                });

            }
        };


        mUsersList.setAdapter(firebaseRecyclerAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Users, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(

                Users.class,
                R.layout.list_user_by_admin,
                AdminUser.UsersViewHolder.class,
                mUsersDatabase

        ) {
            @Override
            protected void populateViewHolder(AdminUser.UsersViewHolder usersViewHolder, Users users, int position) {

                usersViewHolder.setDisplayName(users.getNom());
                usersViewHolder.setUserStatus(users.getPays());
                usersViewHolder.setUserImage(users.getThumb_image(), getApplicationContext());

                final String user_id = getRef(position).getKey();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                final String curenUserUid = user.getUid();

                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        CharSequence options[] = new CharSequence[]{"Modifier", "Supprimer", "Voir profile"};
                        final AlertDialog.Builder builder = new AlertDialog.Builder(AdminUser.this);

                        builder.setTitle("Select Options");

                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {

                                if(i == 0){

                                    Intent chatIntent = new Intent(AdminUser.this, UpdateUserAdmin.class);
                                    chatIntent.putExtra("user_id", user_id);
                                    startActivity(chatIntent);



                                }

                                if(i == 1){


                                    deltetUser(user_id);
                                    dialog.dismiss();

                                }

                                if( i==2){

                                    Intent profileIntent = new Intent(AdminUser.this, userProfilVisiteActivity.class);
                                    profileIntent.putExtra("user_id", user_id);
                                    startActivity(profileIntent);


                                }


                            }
                        });
                        builder.show();






                    }
                });





            }
        };


        mUsersList.setAdapter(firebaseRecyclerAdapter);

    }

    private void deltetUser(String user_id) {


        reference.child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                    Toast.makeText(AdminUser.this, "User supprimer", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(AdminUser.this,"User non supprimer", Toast.LENGTH_LONG).show();
                }
            }
        });


    }



    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayName(String name){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }

        public void setUserStatus(String status){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(status);


        }

        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user2).into(userImageView);

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_serach_users, menu);

        MenuItem item = menu.findItem(R.id.search);
        materialSearchView.setMenuItem(item);

        return true;
    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.search:
//                onSearchRequested();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

}
