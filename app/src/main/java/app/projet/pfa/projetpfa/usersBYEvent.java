package app.projet.pfa.projetpfa;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class usersBYEvent extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    private DatabaseReference mUsersDatabase, databaseReference, databaseReference1;

    private LinearLayoutManager mLayoutManager;

    MaterialSearchView materialSearchView;
    private String inetEventId="";
    ArrayList<String> list = new ArrayList<String>();
    private List<Users> movieList = new ArrayList<>();
    private UsersAdapter mAdapter;
    ArrayList<String> arrayList = new ArrayList<String>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_byevent);

        inetEventId = getIntent().getStringExtra("evId");

        Toast.makeText(usersBYEvent.this, inetEventId, Toast.LENGTH_LONG).show();

        mToolbar = (Toolbar) findViewById(R.id.mytoolbar1);
        setSupportActionBar(mToolbar);


        getSupportActionBar().setTitle("Retrouvez des amis");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        materialSearchView = (MaterialSearchView) findViewById(R.id.mysearch1);
        materialSearchView.clearFocus();
//        materialSearchView.setSuggestions(mUsersList);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReference1 = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReference = FirebaseDatabase.getInstance().getReference().child("EvenementUsers").child(inetEventId);


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    GetIdOfEvenet getIdOfEvenet = new GetIdOfEvenet();

                    String s = postSnapshot.getKey().toString();
                    String ss = dataSnapshot.child(s).getValue().toString();


                    databaseReference1.child(ss).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Users users;
                            String title = dataSnapshot.child("nom").getValue().toString();
                            String lieu = dataSnapshot.child("pays").getValue().toString();




                            users = new Users(title, lieu);

                            mAdapter = new UsersAdapter(movieList);


                            movieList.add(users);


                            mUsersList.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();



                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });





                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mLayoutManager = new LinearLayoutManager(this);

        mUsersList = (RecyclerView) findViewById(R.id.rcv1);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(mLayoutManager);

        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // firebaseUserSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               // firebaseUserSearchDateByProximiteTest();
                return false;
            }
        });
    }

//    public void a()
//    {
//        for (int i=0;i<objects.size(); i++){
//            Log.d("idUsers", String.valueOf(objects.get(i)));
//            //  firebaseUserSearchDateByProximite("-LB2_KcMT0P1L_EQtCnx");
//
//        }
//    }

    private void firebaseUserSearchDateByProximiteTest(List<String>  strings) {


        for (int i=0;i<strings.size(); i++) {
             Log.d("idSelect", list.get(i));
            //  firebaseUserSearchDateByProximite("-LB2_KcMT0P1L_EQtCnx");


            mUsersDatabase.child(strings.get(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    Users users;
                    String title = dataSnapshot.child("nom").getValue().toString();
                    String lieu = dataSnapshot.child("pays").getValue().toString();

                    users = new Users(title, lieu);

                    mAdapter = new UsersAdapter(movieList);


                    movieList.add(users);


                    mUsersList.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();






                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }


    private void firebaseUserSearch(String searchText) {


        Query firebaseSearchQuery = mUsersDatabase.orderByChild("nom").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<Users, usersActivity.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, usersActivity.UsersViewHolder>(

                Users.class,
                R.layout.users_single_layout,
                usersActivity.UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(usersActivity.UsersViewHolder usersViewHolder, Users users, int position) {

                usersViewHolder.setDisplayName(users.getNom());
                usersViewHolder.setUserStatus(users.getPays());
                usersViewHolder.setUserImage(users.getThumb_image(), getApplicationContext());

                final String user_id = getRef(position).getKey();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                final String curenUserUid = user.getUid();

                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(user_id.equals(curenUserUid))
                        {
                            startActivity(new Intent(usersBYEvent.this,ProfileActivity.class));
                        }
                        else{
                            Intent profileIntent = new Intent(usersBYEvent.this, userProfilVisiteActivity.class);
                            profileIntent.putExtra("user_id", user_id);
                            startActivity(profileIntent);
                        }



                    }
                });

            }
        };


        mUsersList.setAdapter(firebaseRecyclerAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

//
//        for(int i=0;i<movieList.size();i++){
//            databaseReference.child(inetEventId).child(movieList.get(i).toString());
//            Log.i("getData",databaseReference.child(inetEventId).child(arrayList.get(i)).toString());
//        }



            FirebaseRecyclerAdapter<Users, usersBYEvent.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, usersBYEvent.UsersViewHolder>(

                    Users.class,
                    R.layout.users_single_layout,
                    usersBYEvent.UsersViewHolder.class,
                    databaseReference1

            ) {
                @Override
                protected void populateViewHolder(usersBYEvent.UsersViewHolder usersViewHolder, Users users, int position) {

                    usersViewHolder.setDisplayName(users.getNom());
                    usersViewHolder.setUserStatus(users.getPays());
                    usersViewHolder.setUserImage(users.getThumb_image(), getApplicationContext());

                    final String user_id = getRef(position).getKey();

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    final String curenUserUid = user.getUid();

                    usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (user_id.equals(curenUserUid)) {
                                startActivity(new Intent(usersBYEvent.this, ProfileActivity.class));
                            } else {
                                Intent profileIntent = new Intent(usersBYEvent.this, userProfilVisiteActivity.class);
                                profileIntent.putExtra("user_id", user_id);
                                startActivity(profileIntent);
                            }


                        }
                    });

                }
            };


            mUsersList.setAdapter(firebaseRecyclerAdapter);


    }


    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayName(String name){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }

        public void setUserStatus(String status){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(status);


        }

        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user2).into(userImageView);

        }


    }

//  public void xx(DatabaseReference databaseReference)
//  {
//
//        FirebaseRecyclerAdapter<Users, usersBYEvent.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, usersBYEvent.UsersViewHolder>(
//
//                Users.class,
//                R.layout.users_single_layout,
//                usersBYEvent.UsersViewHolder.class,
//                databaseReference
//
//        ) {
//            @Override
//            protected void populateViewHolder(usersBYEvent.UsersViewHolder usersViewHolder, Users users, int position) {
//
//                usersViewHolder.setDisplayName(users.getNom());
//                usersViewHolder.setUserStatus(users.getPays());
//                usersViewHolder.setUserImage(users.getThumb_image(), getApplicationContext());
//
//                final String user_id = getRef(position).getKey();
//
//                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//
//                final String curenUserUid = user.getUid();
//
//                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        if(user_id.equals(curenUserUid))
//                        {
//                            startActivity(new Intent(usersBYEvent.this,ProfileActivity.class));
//                        }
//                        else{
//                            Intent profileIntent = new Intent(usersBYEvent.this, userProfilVisiteActivity.class);
//                            profileIntent.putExtra("user_id", user_id);
//                            startActivity(profileIntent);
//                        }
//
//
//
//                    }
//                });
//
//            }
//        };
//
//
//        mUsersList.setAdapter(firebaseRecyclerAdapter);
//
//    }
//
//
//    public static class UsersViewHolder extends RecyclerView.ViewHolder {
//
//        View mView;
//
//        public UsersViewHolder(View itemView) {
//            super(itemView);
//
//            mView = itemView;
//
//        }
//
//        public void setDisplayName(String name){
//
//            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
//            userNameView.setText(name);
//
//        }
//
//        public void setUserStatus(String status){
//
//            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
//            userStatusView.setText(status);
//
//
//        }
//
//        public void setUserImage(String thumb_image, Context ctx){
//
//            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);
//
//            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user2).into(userImageView);
//
//        }
//
//
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_serach_users, menu);

        MenuItem item = menu.findItem(R.id.search);
        materialSearchView.setMenuItem(item);

        return true;
    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.search:
//                onSearchRequested();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}