package app.projet.pfa.projetpfa;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.cete.dynamicpdf.merger.PdfDocument;
import com.cete.dynamicpdf.merger.PdfPage;
import com.cete.dynamicpdf.pageelements.Image;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.cete.dynamicpdf.*;
import com.cete.dynamicpdf.pageelements.Label;
import com.hendrix.pdfmyxml.viewRenderer.AbstractViewRenderer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import android.graphics.Bitmap.Config;

import app.projet.pfa.projetpfa.forum.MainActivity;


public class QRActivit extends AppCompatActivity {
    ImageView imageView;
    Button button;
    TextView textViewN, textViewE, textViewT, textViewD, lieu;
    String EditTextValue;
    Thread thread;
    public final static int QRcodeWidth = 500;
    Bitmap bitmap;
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseReferenceE, databaseReferenceU;
    String getnetnetIdEventQR = "";
    private Button mButton;
    private View mView;
    private static String FILE;
    private ProgressDialog mProgressDialog;
    static String uid="";
    AbstractViewRenderer page;
    static String titleE, dateE, userC = "", emailUserC;
    static String telUserCreater;
    static String nomUseCreater;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);



        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Chargeemnt des données");
        mProgressDialog.setMessage("chargement des informations en cours ");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();




        firebaseAuth = FirebaseAuth.getInstance();

         uid = firebaseAuth.getCurrentUser().getUid();

        FILE = Environment.getExternalStorageDirectory() + "/"+uid+".pdf";

        imageView = (ImageView) findViewById(R.id.imageView);

        textViewN = (TextView) findViewById(R.id.editText);
        textViewE = (TextView) findViewById(R.id.editText2);
        textViewT = (TextView) findViewById(R.id.editText3);
        textViewD = (TextView) findViewById(R.id.editText4);


       // imageView1 = (ImageView) findViewById(R.id.imageViewxxx);

        getnetnetIdEventQR = getIntent().getStringExtra("eventIDQR");


        userC = getIntent().getStringExtra("idOfCretor");
        databaseReferenceU = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReferenceE = FirebaseDatabase.getInstance().getReference().child("Evenement");

        generateFileForIameg();

        databaseReferenceE.child(getnetnetIdEventQR).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String email = dataSnapshot.child("title").getValue().toString();
                titleE = email;
                String nom = dataSnapshot.child("dateDeb").getValue().toString();
                dateE = nom;
                textViewE.setText("L'evenement " + email + " pour la date  " + nom);

//                String idUserCreter = dataSnapshot.child("userCr").getValue().toString();
//                userC = idUserCreter;
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        Toast.makeText(QRActivit.this,""+ userC , Toast.LENGTH_LONG).show();

        databaseReferenceU.child(userC).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String nom = dataSnapshot.child("nom").getValue().toString();
                String email = dataSnapshot.child("email").getValue().toString();
                String tel = dataSnapshot.child("phone").getValue().toString();
                nomUseCreater = nom;
                emailUserC = email;
                telUserCreater = tel;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        databaseReferenceU.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String email = dataSnapshot.child("email").getValue().toString();
                String nom = dataSnapshot.child("nom").getValue().toString();
                String sexe = dataSnapshot.child("sexe").getValue().toString();

                sendEmail(email, "Confirmation de participation", "Confirmation pour votre " +
                        "participation a l 'evenement " + titleE + " le  " + dateE + "\n " +
                        " pour plus information contacter  " + nomUseCreater + " email :  " + emailUserC + " Telephone : " + telUserCreater + ""  );
                if (sexe.equals("Homme")) {
                    textViewN.setText("Monsieur " + nom + " vous avez reçu un email sur cette adresse " + email);
                } else if (sexe.equals("Femme")) {
                    textViewN.setText("Madame " + nom + " vous avez reçu un email sur cette adresse " + email);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(5000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    bitmap = TextToImageEncode(uid);

                                    imageView.setImageBitmap(bitmap);




                                } catch (WriterException e) {
                                    e.printStackTrace();
                                }

//
                                mProgressDialog.dismiss();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();


    }

    private void sendEmail(String email, String subject, String message) {


        //Creating SendMail object
        SendMail sm = new SendMail(this, email, subject, message);

        //Executing sendmail to send email
        sm.execute();
    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor) : getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
//
//    private void sendToPDFFile(ImageView imageView){
//        Document objDocument = new Document();
//        objDocument.setCreator("EventBook");
//        objDocument.setAuthor("Koussai MBIRIKI");
//        objDocument.setTitle("Confirmation");
//
//
//
//        // Create a page to add to the document
//        Page objPage = new Page(PageSize.LETTER, PageOrientation.PORTRAIT,
//                54.0f);
//
//        // Create a Label to add to the page
//        String strText = "Ticket de reservation...\nFrom EventBook \n " ;
//        Label objLabel = new Label(strText, 0, 0, 504, 100,
//                Font.getHelvetica(), 18, TextAlign.CENTER);
//
//
////        try{
////            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_thumb_up_black);
////            ByteArrayOutputStream stream = new ByteArrayOutputStream();
////            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
////            byte[] bitMapData = stream.toByteArray();
////            Image image = new Image.(bitMapData);
////        }catch (Exception e){
////            e.printStackTrace();
////        }
//
//
//
//
//
//
//
//        // Add label to page
//        objPage.getElements().add(objLabel);
//        // Add page to document
//        objDocument.getPages().add(objPage);
//
//        try {
//            // Outputs the document to file
//            objDocument.draw(FILE);
//            Toast.makeText(this, "File has been written to :" + FILE,
//                    Toast.LENGTH_LONG).show();
//            Log.i("oui","oui");
//        } catch (Exception e) {
//            Toast.makeText(this,
//                    "Error, unable to write to file\n" + e.getMessage(),
//                    Toast.LENGTH_LONG).show();
//            Log.i("non","non");
//
//        }
//    }
    @SuppressLint("NewApi")
    public void generateFileForIameg(){

//        Bitmap bitmap = ((BitmapDrawable)imageView1.getDrawable()).getBitmap();

        Drawable drawable = getDrawable(R.drawable.ic_thumb_up_black);
        File file;
        String path = Environment.getExternalStorageDirectory().toString();
        file = new File(path, "UniqueFileName"+".jpg");

        try {
            Bitmap bitmap1;

            bitmap1 = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap1);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            OutputStream stream = null;
            stream = new FileOutputStream(file);
            bitmap1.compress(Bitmap.CompressFormat.JPEG,100,stream);
            stream.flush();
            stream.close();
        } catch (OutOfMemoryError e) {
            // Handle the error
        }catch (IOException e)
        {
            e.printStackTrace();
        }



        Uri savedImageURI = Uri.parse(file.getAbsolutePath());
        Toast.makeText(QRActivit.this,"image est enrgistré sous le répertoire" + savedImageURI, Toast.LENGTH_LONG).show();
        Log.i("pathis", savedImageURI.toString() );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.qr_menu, menu);






        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.pub){

            Intent intent = new Intent(QRActivit.this, MainActivity.class);
            intent.putExtra("eventKey", getnetnetIdEventQR);
            startActivity(intent);
        }

        return true;
    }
}
