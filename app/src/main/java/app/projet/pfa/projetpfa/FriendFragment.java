package app.projet.pfa.projetpfa;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static app.projet.pfa.projetpfa.PrincipaleActivity.string;


/**
 * A simple {@link Fragment} subclass.
 */


public class FriendFragment extends Fragment {

    private View mainView;
    private String current_id_user;
    private RecyclerView recyclerViewFriend;
    private DatabaseReference databaseReference, databaseReferenceUsers;
    private FirebaseAuth firebaseAuth;


    public FriendFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        mainView = inflater.inflate(R.layout.fragment_friend, container, false);

        recyclerViewFriend = (RecyclerView) mainView.findViewById(R.id.listeFirend);
        firebaseAuth = FirebaseAuth.getInstance();
        current_id_user = firebaseAuth.getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Friends").child(current_id_user);
        databaseReference.keepSynced(true);
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReferenceUsers.keepSynced(true);
        recyclerViewFriend.setHasFixedSize(true);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(getContext()));





        return mainView;


    }


    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<Firends, FriendsViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Firends, FriendsViewHolder>(

                Firends.class,
                R.layout.users_single_layout,
                FriendsViewHolder.class,
                databaseReference
        ) {
            @Override
            protected void populateViewHolder(final FriendsViewHolder FviewHolder, Firends friend, int position) {

                FviewHolder.setDate(friend.getDate());

                final  String list_users_id_firend = getRef(position).getKey();

                databaseReferenceUsers.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        final String nom = dataSnapshot.child("nom").getValue().toString();
                        String imageThumb = dataSnapshot.child("thumb_image").getValue().toString();

                        if(dataSnapshot.hasChild("online")) {

                            String userOnline = dataSnapshot.child("online").getValue().toString();
                            FviewHolder.setUserOnlineUpdate(userOnline);

                        }

                        FviewHolder.setName(nom);
                        FviewHolder.setUserImage(imageThumb, getContext());

                        FviewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CharSequence options[] = new CharSequence[]{"Voir Profil", "Envoyer Message"};
                                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                                builder.setTitle("Select Options");

                                builder.setItems(options, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {

                                        if(i == 0){

                                            Intent profileIntent = new Intent(getContext(), userProfilVisiteActivity.class);
                                            profileIntent.putExtra("user_id", list_users_id_firend);
                                            startActivity(profileIntent);

                                        }

                                        if(i == 1){

                                            Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                            chatIntent.putExtra("user_id", list_users_id_firend);
                                            chatIntent.putExtra("user_name", nom);
                                            startActivity(chatIntent);

                                        }


                                    }
                                });
                                builder.show();

                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        };

        recyclerViewFriend.setAdapter(firebaseRecyclerAdapter);
    }

    public static class FriendsViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public FriendsViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setDate(String date) {

            TextView textView = (TextView) mView.findViewById(R.id.user_single_status);
            textView.setText(date);
        }

        public void setName(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.user_single_name);
            textView.setText(name);
        }

        public void setUserImage(String thumb_image, Context ctx) {

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user2).into(userImageView);

        }

        public void setUserOnlineUpdate(String on) {

            ImageView imageView = (ImageView) mView.findViewById(R.id.user_single_online_icon);

            if (on.equals("true")) {
                imageView.setVisibility(View.VISIBLE);
            } else {
                imageView.setVisibility(View.INVISIBLE);

            }


        }

    }
}
