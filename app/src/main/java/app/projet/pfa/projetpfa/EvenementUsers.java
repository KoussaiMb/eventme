package app.projet.pfa.projetpfa;

import java.util.ArrayList;

public class EvenementUsers {
    private String eventID;

    public EvenementUsers() {
    }

    public EvenementUsers(String eventID) {
        this.eventID = eventID;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }
}
