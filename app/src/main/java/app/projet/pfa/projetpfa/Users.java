package app.projet.pfa.projetpfa;

import android.renderscript.Short4;

/**
 * Created by koussai on 24/03/2018.
 */

public class Users {

    private String nom;
    private String age;
    private String sexe;
    private String ville;
    private String pays;
    private String image;
    private String thumb_image;
    private String phone;
    private String email;
    private String device_token;


    public Users() {
    }

    public Users(String nom, String pays) {
        this.nom = nom;
        this.pays = pays;
    }

    public Users(String nom, String age, String sexe, String ville, String pays, String image, String thumb_image, String phone, String email, String device_token) {
        this.nom = nom;
        this.age = age;
        this.sexe = sexe;
        this.ville = ville;
        this.pays = pays;
        this.image = image;
        this.thumb_image = thumb_image;
        this.phone = phone;
        this.email = email;
        this.device_token = device_token;
    }


    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }





    @Override
    public String toString() {
        return "Users{" +
                "nom='" + nom + '\'' +
                ", age='" + age + '\'' +
                ", sexe='" + sexe + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", image='" + image + '\'' +
                ", thumb_image='" + thumb_image + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +

                '}';
    }
}
