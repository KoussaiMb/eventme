package app.projet.pfa.projetpfa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.cete.dynamicpdf.io.T;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import app.projet.pfa.projetpfa.GetLocationAdress.MapsActivity;
import app.projet.pfa.projetpfa.forum.MainActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class viewEventActivity extends AppCompatActivity {

    private TextView textViewTitle, textViewDateDeb, textViewHeureDeb, textViewDateFin, textViewUser, textViewLoc, textViewCatg, textViewType;
    private MultiAutoCompleteTextView textDesc;
    private ImageView imageView;
    private DatabaseReference databaseReferenceEvenet, databaseReferenceUsers,databaseReference,databaseReferenceGoogleMaps, databaseReferenceCheck;
    private ProgressDialog mProgressDialog;
    private static String nameUserCreter;
    private static String phoneUserCreter;
    private TextView textViewValider;
    private ImageView imageViewValider;
    private static String idUserCreter;
    private static String bodyMsg;
    private static String id_user_firend;
    FloatingActionButton flt, flt1;
    TextView tSlide, tForum;
    List<String> userIdList = new ArrayList();
    public static String passingDataLat;
    private static String passingDataLang;
    private static String url;
    private static String passingEventId;
    static String getUserCurentID;
    private TextView textViewPayer;
    ImageView vSlide, vForum;
    ImageView imageViewPayer, voirPart;
    DatabaseReference database, databaseReferenceChangeEtat;
    static String idUserC, idUserCur, userCre;
    boolean check ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);

        Toast.makeText(viewEventActivity.this, passingDataLat, Toast.LENGTH_LONG).show();


                         FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                         final String curenUserUid = user.getUid();
        getUserCurentID = curenUserUid;

        idUserCur = curenUserUid;

//        String test = "oUOoX0WvL7Pid7VSGUaoxohnEBd2";
//        if(test.equals(getUserCurentID))
//        {
//            Toast.makeText(viewEventActivity.this, "oui", Toast.LENGTH_LONG).show();
//
//        }else{
//            Toast.makeText(viewEventActivity.this, "non", Toast.LENGTH_LONG).show();
//
//        }


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Chargeemnt des données");
        mProgressDialog.setMessage("chargement des informations en cours ");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        textViewTitle = (TextView)findViewById(R.id.textView5);
        textViewDateDeb = (TextView)findViewById(R.id.textView6);
        textViewHeureDeb = (TextView)findViewById(R.id.textView9);
        textViewDateFin = (TextView)findViewById(R.id.textView2);
        textViewCatg = (TextView)findViewById(R.id.textView4);
        textViewUser = (TextView)findViewById(R.id.groupname);
        textViewLoc = (TextView)findViewById(R.id.textlocation);
        textViewValider = (TextView)findViewById(R.id.textValider);
        textViewType = (TextView)findViewById(R.id.textView4444);
        imageViewValider = (ImageView)findViewById(R.id.imageValider);
        textDesc = (MultiAutoCompleteTextView)findViewById(R.id.textDesc);
        imageView = (ImageView)findViewById(R.id.call);
        textViewPayer = (TextView)findViewById(R.id.msg);
        imageViewPayer = (ImageView)findViewById(R.id.x);
        textViewPayer.setVisibility(View.INVISIBLE);
        imageViewPayer.setVisibility(View.INVISIBLE);
        voirPart = (ImageView)findViewById(R.id.voirPart);
        vSlide = (ImageView)findViewById(R.id.imageSlide);
        vForum = (ImageView)findViewById(R.id.imageForum);
        tSlide = (TextView)findViewById(R.id.textSlide);
        tForum = (TextView)findViewById(R.id.textForm);








        textDesc.setEnabled(false);


        final String event_id = getIntent().getStringExtra("event_id");

        passingEventId = event_id;

//        Toast.makeText(viewEventActivity.this, "monEvenet1 " +passingEventId, Toast.LENGTH_LONG ).show();
//        Toast.makeText(viewEventActivity.this, "monEvenet2 " +event_id, Toast.LENGTH_LONG ).show();



//        flt =(FloatingActionButton)findViewById(R.id.flt);
        vSlide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Voir Image", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();


                Intent intent = new Intent(viewEventActivity.this, SlideActivity.class);
                intent.putExtra("eventKey", event_id);
                startActivity(intent);




            }
        });


//        flt1 =(FloatingActionButton)findViewById(R.id.flt1);
        vForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Voir Image", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();


                Intent intent = new Intent(viewEventActivity.this, MainActivity.class);
                intent.putExtra("eventKey", event_id);
                startActivity(intent);




            }
        });


        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement").child(event_id);
        database = FirebaseDatabase.getInstance().getReference().child("EvenementUsers");


        checkIfPart(event_id, getUserCurentID);





        voirPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewEventActivity.this, usersBYEvent.class);
                intent.putExtra("evId", event_id);
                startActivity(intent);
            }
        });




        databaseReference = FirebaseDatabase.getInstance().getReference().child("Friends").child(curenUserUid);

        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");

        databaseReferenceGoogleMaps = FirebaseDatabase.getInstance().getReference().child("Evenement").child(event_id);


        databaseReferenceEvenet.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String title = dataSnapshot.child("title").getValue().toString();
                String dateDeb = dataSnapshot.child("dateDeb").getValue().toString();
                String heureDeb = dataSnapshot.child("heureDeb").getValue().toString();
                String dateFin = dataSnapshot.child("dateFin").getValue().toString();
                String heureFin = dataSnapshot.child("heureFin").getValue().toString();
                String usersCreater = dataSnapshot.child("userCr").getValue().toString();
                String location = dataSnapshot.child("lieu").getValue().toString();
                String categorie = dataSnapshot.child("categorie").getValue().toString();
                String description = dataSnapshot.child("description").getValue().toString();
                String type = dataSnapshot.child("type").getValue().toString();
                String sousDateDeb = dateDeb.substring(14);
                String sousHeureDeb = heureDeb.substring(16);
                String sousDateFin = dateFin.substring(12);
                String sousHeureFin = heureFin.substring(14);
                String dateFinHeureFin = sousDateFin + " , " + sousHeureFin;
                idUserCreter = dataSnapshot.child("userCr").getValue().toString();
                url = dataSnapshot.child("key").getValue().toString();

                idUserC = idUserCreter;
                userCre = idUserCreter;
                if(usersCreater.equals(curenUserUid)){
                    imageViewValider.setVisibility(View.GONE);
                    textViewValider.setVisibility(View.GONE);
                    check = true;
                }

//                database.child("eventID").setValue(event_id);
//                database.child("userCreater").setValue(usersCreater);

//                database.child(event_id).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
//
//                            String s = postSnapshot.getKey().toString();
//
//                            Log.i("myvarLogs",s);
//
//
//                            String ss = dataSnapshot.child(s).getValue().toString();
//
//                            Log.i("ssdata",ss);
//
//
//
//                            Log.i("user", curenUserUid);
//
//                            if(s.equals(getUserCurentID))
//                            {
//                                textViewValider.setTextColor(Color.RED);
//                                textViewValider.setText("Anuuler");
//                                imageViewValider.setImageResource(R.drawable.ic_thumb_down_black);
//                                imageViewValider.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        database.child(event_id).child(getUserCurentID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
//                                            @Override
//                                            public void onComplete(@NonNull Task<Void> task) {
//
//                                                if(task.isSuccessful())
//                                                    Toast.makeText(viewEventActivity.this,"oui", Toast.LENGTH_LONG).show();
//
//                                            }
//                                        });
//
//                                    }
//                                });
//
//                            }if(!s.equals(getUserCurentID))
//                            {
//                                textViewValider.setTextColor(Color.BLUE);
//                                textViewValider.setText("Participer");
//                                imageViewValider.setImageResource(R.drawable.ic_thumb_up_black);
//                                imageViewValider.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        database.child(event_id).child(getUserCurentID).setValue(getUserCurentID).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                            @Override
//                                            public void onComplete(@NonNull Task<Void> task) {
//
//                                                if(task.isSuccessful())
//                                                {
//                                                    Toast.makeText(viewEventActivity.this,"oui", Toast.LENGTH_LONG).show();
//                                                    Intent intent = new Intent(viewEventActivity.this, QRActivit.class);
//                                                    intent.putExtra("eventIDQR", event_id);
//                                                    startActivity(intent);
//
//                                                }
//
//                                            }
//                                        });
//
//                                    }
//                                });
//                            }
//
//
//
//                        }
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });







                databaseReferenceUsers.child(usersCreater).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        if(dataSnapshot == null)
                        {
                            nameUserCreter = "admin";
                            phoneUserCreter =  "admin";
                        }else{
                            nameUserCreter = dataSnapshot.child("nom").getValue().toString();
                            phoneUserCreter = dataSnapshot.child("phone").getValue().toString();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                textViewTitle.setText("     "+title);
                textViewDateDeb.setText(" "+sousDateDeb);
                textViewHeureDeb.setText(" "+sousHeureDeb);
                textViewDateFin.setText(" "+dateFinHeureFin);
                textViewCatg.setText(" "+categorie);
                textViewLoc.setText(location);
                textDesc.setText(description);
                textViewType.setText(type);

                if (type.equals("Payant"))
                {
                    textViewPayer.setVisibility(View.VISIBLE);
                    imageViewPayer.setVisibility(View.VISIBLE);
                    textViewPayer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(viewEventActivity.this,PayementActivity.class));
                        }
                    });
                }


//                Thread mThread = new Thread() {
//                    @Override
//                    public void run() {
//                        textViewUser.setText(" "+nameUserCreter);
//
//                        mProgressDialog.dismiss();
//                    }
//                };
//                mThread.start();

                Thread thread = new Thread(){
                    @Override
                    public void run() {
                        try {
                            synchronized (this) {
                                wait(5000);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        textViewUser.setText(" "+nameUserCreter);
                                        WebView wv = (WebView) findViewById(R.id.webview);
                                        wv.setWebViewClient(new Callback());
                                        WebSettings webSettings = wv.getSettings();
                                        webSettings.setBuiltInZoomControls(true);
                                        wv.loadUrl(url);
//
                        mProgressDialog.dismiss();
                                    }
                                });

                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    };
                };
                thread.start();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        databaseRef.child("Friends").child(curenUserUid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot==null)return;
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    userIdList.add(postSnapshot.getKey());


                }
            }

            @Override
            public void onCancelled(DatabaseError   databaseError) {
                // Error
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[]{"Appel vocal (payant)", "Envoyer SMS (payant)", "Envoyer Message"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());


                for (String object: userIdList) {
                    Log.d("id", object);
                }

//                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//
//                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
////                            User user = snapshot.getValue(User.class);
////                            System.out.println(user.email);
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });




                builder.setTitle("Select option");

                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {

                        if(i==0){
                            try{
                                Uri uri = Uri.parse("tel:"+phoneUserCreter);
                                Intent appel = new Intent(Intent.ACTION_CALL, uri);
                                startActivity(appel);
                            }catch (SecurityException e){
                                e.printStackTrace();
                            }

                        }

                        if(i==1){




                            LayoutInflater inflater = LayoutInflater.from(viewEventActivity.this);
                            View subView = inflater.inflate(R.layout.alertdialog_custom_view, null);
                            final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText);
                            final ImageView subImageView = (ImageView)subView.findViewById(R.id.image);
                            Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
                            subImageView.setImageDrawable(drawable);

                            AlertDialog.Builder builder = new AlertDialog.Builder(viewEventActivity.this);
                            builder.setTitle("SMS");
                            builder.setMessage("Taper Votre Message");
                            builder.setView(subView);
                            AlertDialog alertDialog = builder.create();

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String body=subEditText.getText().toString();
                                    try{

                                        SmsManager smsManager = SmsManager.getDefault();
                                        smsManager.sendTextMessage(phoneUserCreter.toString(),null,body.toString(),null,null);
                                        Toast.makeText(viewEventActivity.this,"msg envoyé" ,Toast.LENGTH_LONG).show();

                                    }catch (Exception ex)
                                    {
                                        Toast.makeText(viewEventActivity.this,ex.getMessage(),Toast.LENGTH_LONG).show();

                                        ex.printStackTrace();
                                    }
                                }
                            });

                            builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                            builder.show();









                        }

                        if(i==2){
                            if(idUserCreter.equals(curenUserUid))
                            {
                                startActivity(new Intent(viewEventActivity.this,ProfileActivity.class));

                            }else  if(!userIdList.contains(idUserCreter)){
                                Intent profileIntent = new Intent(viewEventActivity.this, userProfilVisiteActivity.class);
                                profileIntent.putExtra("user_id", idUserCreter);
                                startActivity(profileIntent);
                            }else if(userIdList.contains(idUserCreter)){
                                Intent chatIntent = new Intent(viewEventActivity.this, ChatActivity.class);
                                chatIntent.putExtra("user_id", idUserCreter);
                                chatIntent.putExtra("user_name", nameUserCreter);
                                startActivity(chatIntent);
                            }






                        }

                    }
                });

                builder.show();


            }
        });

//
//        String getIDCurrent = FirebaseAuth.getInstance().getCurrentUser().getUid().toString();
//        Toast.makeText(viewEventActivity.this, getIDCurrent, Toast.LENGTH_LONG).show();
//        Toast.makeText(viewEventActivity.this, idUserCreter, Toast.LENGTH_LONG).show();

        textViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                databaseReferenceGoogleMaps.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//
//                        passingDataLat = dataSnapshot.child("lat").getValue().toString();
//                        passingDataLang = dataSnapshot.child("lang").getValue().toString();
//                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(viewEventActivity.this);
//                        SharedPreferences.Editor editor = preferences.edit();
//                        editor.putString("latData",passingDataLat);
//                        editor.putString("langData",passingDataLang);
//                        editor.commit();
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });
//
//
//                Log.d("lat",""+passingDataLat);
//                Log.d("lang",""+passingDataLang);
//                Intent intent = new Intent(viewEventActivity.this, MapsActivity.class);
////                String x ="test";
////                intent.putExtra("njareb",x);
////              //  intent.putExtra("lang",passingDataLang);
//                startActivity(intent);
            }
        });







    }

    private void checkIfPart(final String idE, final String idU) {

        databaseReferenceChangeEtat = FirebaseDatabase.getInstance().getReference().child("EvenementUsers");

        databaseReferenceChangeEtat.child(idE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    String s = postSnapshot.getKey().toString();


                    String ss = dataSnapshot.child(s).getValue().toString();

                    if(ss.equals(idU)) {
//                        Toast.makeText(viewEventActivity.this, "current " + getUserCurentID, Toast.LENGTH_LONG).show();
//                        Toast.makeText(viewEventActivity.this, "myS " + s, Toast.LENGTH_LONG).show();
//                        Toast.makeText(viewEventActivity.this, "ssMy " + ss, Toast.LENGTH_LONG).show();
//
//                        Log.i("currentUser", getUserCurentID);
//                        Log.i("myS", s);
//                        Log.i("ssMy", ss);


                        tForum.setVisibility(View.VISIBLE);
                        vForum.setVisibility(View.VISIBLE);
                        vForum.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                Intent intent = new Intent(viewEventActivity.this, MainActivity.class);
                                intent.putExtra("eventKey", idE);
                                startActivity(intent);
                            }
                        });

                        textViewValider.setTextColor(Color.RED);
                        textViewValider.setText("Anuuler");
                        imageViewValider.setImageResource(R.drawable.ic_thumb_down_black);
                        imageViewValider.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                databaseReferenceChangeEtat.child(idE).child(idU).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if(task.isSuccessful()){
                                                Toast.makeText(viewEventActivity.this, "participation annulée", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            }
                        });
                        return;
                    }else {
                        vForum.setVisibility(View.GONE);
                        tForum.setVisibility(View.GONE);
                        textViewValider.setTextColor(Color.BLUE);
                        textViewValider.setText("Participer");
                        imageViewValider.setImageResource(R.drawable.ic_thumb_up_black);


                        imageViewValider.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                databaseReferenceChangeEtat.child(idE).child(getUserCurentID).setValue(getUserCurentID).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if(task.isSuccessful())

                                        {
                                            Intent intent = new Intent(viewEventActivity.this, QRActivit.class);
                                            intent.putExtra("eventIDQR", idE);
                                            intent.putExtra("idOfCretor",idUserCreter);
                                           // Toast.makeText(viewEventActivity.this,"oui", Toast.LENGTH_LONG).show();
                                            startActivity(intent);

                                        }
                                    }
                                });

                            }
                        });
                    }


//
//
// }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private void qrToUser() {

        startActivity(new Intent(viewEventActivity.this, QRActivit.class));
    }

    public  void  sendMessagetoUserCreter(){


        }


    private class Callback  extends WebViewClient {

        @Override

        public boolean shouldOverrideUrlLoading(WebView view, String url) {

//            if (Uri.parse(url).getHost().equals("www.facebook.com")) {
//
//                return false;
//
//            }
//
//
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//
//            startActivity(intent);
//
//            return true;
//        }
            return (false);
        }
    }

    private void deleteChild(){
        databaseReferenceEvenet.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(viewEventActivity.this, "supprimer avec succees", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(viewEventActivity.this, RequeteFragment.class));
                }else{
                    Toast.makeText(viewEventActivity.this, "errure" + task.getException(), Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

            getMenuInflater().inflate(R.menu.event_menu, menu);






        return super.onCreateOptionsMenu(menu);
    }





    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        final MenuItem menuItem = menu.findItem(R.id.supp);
        final MenuItem menuItem2 = menu.findItem(R.id.mod);

        databaseReferenceCheck = FirebaseDatabase.getInstance().getReference().child("Evenement").child(passingEventId);

        databaseReferenceCheck.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String useridCr = dataSnapshot.child("userCr").getValue().toString();

                if(useridCr.equals(getUserCurentID)){
                    menuItem.setVisible(true);
                    menuItem2.setVisible(true);
                }else{
                    menuItem.setVisible(false);
                    menuItem2.setVisible(false);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.mapVoir){

            databaseReferenceGoogleMaps.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    passingDataLat = dataSnapshot.child("lat").getValue().toString();
                    passingDataLang = dataSnapshot.child("lang").getValue().toString();
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(viewEventActivity.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("latData",passingDataLat);
                    editor.putString("langData",passingDataLang);
                    editor.commit();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            Log.d("lat",""+passingDataLat);
            Log.d("lang",""+passingDataLang);
            Intent intent = new Intent(viewEventActivity.this, MapsActivity.class);
//                String x ="test";
//                intent.putExtra("njareb",x);
//              //  intent.putExtra("lang",passingDataLang);
            startActivity(intent);
        }










        if(id == R.id.supp){

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            deleteChild();

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(viewEventActivity.this);
            builder.setMessage("Vous voulez vraiment supprimer ?").setPositiveButton("Oui", dialogClickListener)
                    .setNegativeButton("Non", dialogClickListener).show();
        }

        if (id == R.id.mod) {
            Intent modEvent = new Intent(viewEventActivity.this, updateEvent.class);
            modEvent.putExtra("evnt_id", passingEventId);
            startActivity(modEvent);

        }


        if (id == R.id.meteo) {
            Intent modEvent = new Intent(viewEventActivity.this, RegisterActivity.class);
            modEvent.putExtra("evnt_id", passingEventId);
            startActivity(modEvent);

        }


        return true;
    }

//    public void checkEquals(String str, String str1){
//        if(str.equals(str1))
//        {
//            textViewValider.setText("Annuler");
//            textViewValider.setTextColor(Color.RED);
//            imageViewValider.setImageResource(R.drawable.ic_thumb_down_black);
//        }else{
//            textViewValider.setText("Participer");
//            textViewValider.setTextColor(Color.BLUE);
//            imageViewValider.setImageResource(R.drawable.ic_thumb_up_black);
//
//        }
//
//
//    }

    public  void add(final String event){

        database = FirebaseDatabase.getInstance().getReference().child("EvenementUsers").child(event);

        textViewValider.setTextColor(Color.BLUE);
        textViewValider.setText("Participer");
        imageViewValider.setImageResource(R.drawable.ic_thumb_up_black);
        imageViewValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.child(event).child(getUserCurentID).setValue(getUserCurentID).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){

                        }
                           // Toast.makeText(viewEventActivity.this,"bien participé", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

    }

    public  void remove(final String event){

        database = FirebaseDatabase.getInstance().getReference().child("EvenementUsers").child(event);

        textViewValider.setTextColor(Color.RED);
        textViewValider.setText("Anuuler");
        imageViewValider.setImageResource(R.drawable.ic_thumb_down_black);
        imageViewValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.child(event).child(getUserCurentID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){

                        }
                           // Toast.makeText(viewEventActivity.this,"oui", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

    }
}

