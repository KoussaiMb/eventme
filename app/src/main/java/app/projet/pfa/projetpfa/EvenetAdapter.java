package app.projet.pfa.projetpfa;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class EvenetAdapter extends RecyclerView.Adapter<EvenetAdapter.MyViewHolder> {

    private List<Evenement> moviesList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_event_aproximite_test, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Evenement movie = moviesList.get(position);
        holder.textView22.setText(movie.getTitle());
        holder.textView33.setText(movie.getDateDeb());
        holder.textView44.setText(movie.getLieu());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView22, textView33, textView44 ;

        public MyViewHolder(View view) {
            super(view);
            textView22 = (TextView) view.findViewById(R.id.textView22);
            textView33 = (TextView) view.findViewById(R.id.textView33);
            textView44 = (TextView) view.findViewById(R.id.textView44);
        }
    }

    public EvenetAdapter(List<Evenement> moviesList) {
        this.moviesList = moviesList;
    }

}
