package app.projet.pfa.projetpfa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class PrincipaleActivity extends AppCompatActivity  {

   // private TextView mTextMessage;
    private ViewPager viewPager;

    private PagesAdapter pagesAdapter;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    protected  static  String string="";
    private LinearLayoutManager mLayoutManager;
    DatabaseReference  mUsersDatabase;




    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    diplayBoiteDialog();
                    return true;
                case R.id.navigation_dashboard:
                    if(viewPager.getCurrentItem() == 0)
                    {
                        startActivity(new Intent(getApplicationContext(),serachEventActivity.class));
                        break;
                    }

                    if(viewPager.getCurrentItem() == 1)
                    {
                        startActivity(new Intent(getApplicationContext(),usersActivity.class));
                        break;
                    }



                case R.id.navigation_notifications:
                   startActivity(new Intent(getApplicationContext(),usersActivity.class));
                   break;
            }
            return true;
        }
    };

    private void diplayBoiteDialog() {
        CharSequence options[] = new CharSequence[]{"Evenement a proximté"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(PrincipaleActivity.this);

        builder.setTitle("Select Options");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                if(i == 0){
                    string ="oui";
                    viewPager.setCurrentItem(0);



                }



            }
        });
        builder.show();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);

       // mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Evenement");


        viewPager = (ViewPager) findViewById(R.id.viewPager);
        pagesAdapter = new PagesAdapter(getSupportFragmentManager());

        viewPager.setAdapter(pagesAdapter);

        tabLayout = (TabLayout)findViewById(R.id.main_tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(1);








    }



    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.proximiti_menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int x = item.getItemId();
//
//        if(x == R.id.choix)
//            diplayBoiteDialog();
//
//        return true;
//    }






}
