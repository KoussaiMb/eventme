package app.projet.pfa.projetpfa;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;

import app.projet.pfa.projetpfa.Model.OpenWeatherMap;
import app.projet.pfa.projetpfa.Model.Main;
import app.projet.pfa.projetpfa.Common;
import app.projet.pfa.projetpfa.Helper;
import de.hdodenhof.circleimageview.CircleImageView;


import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class RegisterActivity extends AppCompatActivity {
//    LoginButton loginButton;
//    CallbackManager callbackManager;

    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, updatedField;
    ImageView imageViewIcone;
    CircleImageView circleImageView;
    String getEventId;
    DatabaseReference databaseReferenceEvenet;
    private ProgressDialog mProgressDialog;
    static String lat , lang ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_weather);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Chargeemnt des données");
        mProgressDialog.setMessage("chargement des informations en cours ");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        getEventId = getIntent().getStringExtra("evnt_id");

        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement").child(getEventId);


        cityField = (TextView)findViewById(R.id.city_field);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView)findViewById(R.id.current_temperature_field);
        humidity_field = (TextView)findViewById(R.id.humidity_field);
        pressure_field = (TextView)findViewById(R.id.pressure_field);
        circleImageView = (CircleImageView)findViewById(R.id.imageView5);



        final Weather.placeIdTask asyncTask =new Weather.placeIdTask(new Weather.AsyncResponse() {
            public void processFinish(final String weather_city, final String weather_description, final String weather_temperature, final String weather_humidity, final String weather_pressure, final String weather_updatedOn, final String weather_icon) {


                Thread thread = new Thread(){
                    @Override
                    public void run() {
                        try {
                            synchronized (this) {
                                wait(5000);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cityField.setText(weather_city);
                                        updatedField.setText(weather_updatedOn);
                                        detailsField.setText(weather_description);
                                        currentTemperatureField.setText(weather_temperature);
                                        humidity_field.setText("Humidity: "+weather_humidity);
                                        pressure_field.setText("Pressure: "+weather_pressure);
                                        Picasso.with(getApplicationContext()).load(weather_icon).into(circleImageView);
                                        mProgressDialog.dismiss();
                                    }
                                });

                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    };
                };
                thread.start();

            }
        });

        Toast.makeText(RegisterActivity.this,lat + " " + lang, Toast.LENGTH_LONG ).show();
        databaseReferenceEvenet.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                lat = dataSnapshot.child("lat").getValue().toString();
                lang = dataSnapshot.child("lang").getValue().toString();
                asyncTask.execute(lat, lang);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //  asyncTask.execute("Latitude", "Longitude")



    }
}
