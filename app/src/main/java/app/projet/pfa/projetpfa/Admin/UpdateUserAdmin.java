package app.projet.pfa.projetpfa.Admin;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Calendar;
import java.util.HashMap;

import app.projet.pfa.projetpfa.R;
import app.projet.pfa.projetpfa.Users;

public class UpdateUserAdmin extends AppCompatActivity implements  View.OnClickListener{

    private AutoCompleteTextView chEmail,psedeo;
    private EditText chPass;
    private Button btnInsc,btnFb,btnGoogle;
    private ProgressDialog progressDialog;
    private FirebaseAuth auth;
    static  String displayName;
    public static String passingEmailuser;
    public static String device_token;
    private EditText editTextPays, editTextPhone, editText;
    private TextView textViewDateNaissance;
    private RadioGroup radioGroup;
    private RadioButton radioButtonH,radioButtonF;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String dateNaissance = "";
    String sexe = "";


    private DatabaseReference firebaseDatabase, databaseReference;

    static String idUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user_admin);

        idUser = getIntent().getStringExtra("user_id");

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReference.child(idUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String nom ;
                String sexe;
                String dataNa;
                String telepgone;
                String emailUp;
                String pawd;
                String pays;

                nom = dataSnapshot.child("nom").getValue().toString();
                sexe = dataSnapshot.child("sexe").getValue().toString();
                dataNa = dataSnapshot.child("age").getValue().toString();
                telepgone = dataSnapshot.child("phone").getValue().toString();
                emailUp = dataSnapshot.child("email").getValue().toString();
                pawd ="123456";
                pays = dataSnapshot.child("pays").getValue().toString();

                psedeo.setText(nom);
                chEmail.setText(emailUp);
                chPass.setText(pawd);
                editTextPhone.setText(telepgone);
                psedeo.setText(nom);
                editTextPays.setText(pays);
                textViewDateNaissance.setText(dataNa);

                if(sexe.equals("Homme")){

                    radioGroup.check(R.id.homme);
                   }else if(sexe.equals("Femme")) {
                    radioGroup.check(R.id.Femme);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        chEmail = (AutoCompleteTextView) findViewById(R.id.email);
        psedeo = (AutoCompleteTextView) findViewById(R.id.psedeo);
        chPass = (EditText) findViewById(R.id.create_new_password);
        editTextPays = (EditText) findViewById(R.id.pays);
        editTextPhone = (EditText) findViewById(R.id.Phone);
        textViewDateNaissance = (TextView)findViewById(R.id.dateN);
        radioGroup = (RadioGroup)findViewById(R.id.sexeRg);

        btnInsc = (Button) findViewById(R.id.btnInsc);
        // btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnInsc.setOnClickListener(this);

        textViewDateNaissance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                String test="Date de début ";

                DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dateNaissance =""+dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        progressDialog = new ProgressDialog(this);

        auth = FirebaseAuth.getInstance();

    }

    private void registerUser() {
        final String email = chEmail.getText().toString().trim();
        final String password = chPass.getText().toString().trim();
        final String nom = psedeo.getText().toString().trim();
        final String pays = editTextPays.getText().toString().trim();
        final String phone = editTextPhone.getText().toString().trim();

        int selectedRadioButtonID = radioGroup.getCheckedRadioButtonId();

        if(selectedRadioButtonID == -1)
        {
            Toast.makeText(UpdateUserAdmin.this,"champs obligatoire", Toast.LENGTH_LONG).show();
            return;

        }
        else {
            RadioButton selectedRadioButton = (RadioButton) findViewById(selectedRadioButtonID);
            sexe = selectedRadioButton.getText().toString();
        }







        if (dateNaissance.isEmpty()) {
            editTextPays.setError("champs date de naissance est obligatoire");
            editTextPays.requestFocus();
            return;
        }

        if (pays.isEmpty()) {
            editTextPays.setError("champs pays est obligatoire");
            editTextPays.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            editTextPhone.setError("champs phone est obligatoire");
            editTextPhone.requestFocus();
            return;
        }

        if (phone.length() != 8 ) {
            chPass.setError("Min 8 carcateres ou nombres");
            chPass.requestFocus();
            return;
        }


        if (email.isEmpty()) {
            chEmail.setError("champs email est obligatoire");
            chEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            chEmail.setError("SVP entrez un email valide");
            chEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            chPass.setError("Mot de passe est obligatoire ");
            chPass.requestFocus();
            return;
        }

        if (password.length() < 6) {
            chPass.setError("Min 6 carcateres ou nombres");
            chPass.requestFocus();
            return;
        }

        if(nom.isEmpty()){
            psedeo.setError("champs nom est obligatoire");
            chEmail.requestFocus();
            return;
        }

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(idUser);
//

                    device_token = FirebaseInstanceId.getInstance().getToken();

                    HashMap<String, String> hashMapUser = new HashMap<>();
                    hashMapUser.put("email",email);
                    //passingEmail=email.toString();
                    hashMapUser.put("nom",nom);
                    hashMapUser.put("sexe",sexe);
                    hashMapUser.put("age",dateNaissance);
                    hashMapUser.put("pays",pays);
                    hashMapUser.put("phone",phone);
                    hashMapUser.put("ville","ville");
                    hashMapUser.put("image","default");
                    hashMapUser.put("thumb_image","default");
                    hashMapUser.put("device_token",device_token);
        databaseReference.child(idUser).setValue(hashMapUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){
                              Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
                              Intent intent = new Intent(UpdateUserAdmin.this, AdminUser.class);
                              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                              startActivity(intent);
                              finish();
                }else {
                    Toast.makeText(getApplicationContext(), "utilisateur non inscrit", Toast.LENGTH_LONG).show();

                }
            }
        });


        //progressBar.setVisibility(View.VISIBLE);

//        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                //progressBar.setVisibility(View.GONE);
//                if (task.isSuccessful()) {
//                    passingEmailuser =email;
//
//                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//
//
//
//                    String uid = user.getUid();
//
//                    firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
//
//                    device_token = FirebaseInstanceId.getInstance().getToken();
//
//                    HashMap<String, String> hashMapUser = new HashMap<>();
//                    hashMapUser.put("email",email);
//                    //passingEmail=email.toString();
//                    hashMapUser.put("nom",nom);
//                    hashMapUser.put("sexe",sexe);
//                    hashMapUser.put("age",dateNaissance);
//                    hashMapUser.put("pays",pays);
//                    hashMapUser.put("phone",phone);
//                    hashMapUser.put("ville","ville");
//                    hashMapUser.put("image","default");
//                    hashMapUser.put("thumb_image","default");
//                    hashMapUser.put("device_token",device_token);
//
//
//
//                    Users users = new Users(nom,dateNaissance,sexe,"ville", pays, "default","default", phone, email, device_token);
//
////                    firebaseDatabase.setValue(hashMapUser).addOnCompleteListener(new OnCompleteListener<Void>() {
////                        @Override
////                        public void onComplete(@NonNull Task<Void> task) {
////                            if(task.isSuccessful())
////                            {
////                              Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
////                              Intent intent = new Intent(InscritActivity.this, DashbordActivity.class);
////                              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////                              startActivity(intent);
////                              finish();
////                            }
////                        }
////                    });
//
//                    firebaseDatabase.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            if(task.isSuccessful())
//                            {
//                                Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
//                                Intent intent = new Intent(UpdateUserAdmin.this, AdminUser.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    });
//
//
//
//
//
//                } else {
//
//                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
//                        Toast.makeText(getApplicationContext(), "utilisateur deja inscrit", Toast.LENGTH_LONG).show();
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
//                    }
//
//                }
//            }
//        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInsc:
                registerUser();
                break;


        }
    }
}

