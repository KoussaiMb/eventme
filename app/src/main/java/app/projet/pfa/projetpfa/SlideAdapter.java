package app.projet.pfa.projetpfa;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SlideAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SlideAdapter(Context context) {
        this.context = context;
    }

    public int[] slides_images = {
            R.drawable.ic_thumb_down_black,
            R.drawable.ic_action_action_search,
            R.drawable.ic_alarm_black_24dp

    };


    public String[] slides_heading = {
            "a",
            "b",
            "c"
    };

    public String[] slide_desc = {
            "fdskjsdfkljdskfklmdfjjjjjjjjjjjjjkmjfdslkfdslkjklfdjsjklfdsjkl" +
                    "dsqlkqsdqsdqslqsdjlsqdl"+
                       "dsjsdlfjdfslksdfl",
            "fdskjsdfkljdskfklmdfjjjjjjjjjjjjjkmjfdslkfdslkjklfdjsjklfdsjkl" +
                    "dsqlkqsdqsdqslqsdjlsqdl"+
                    "dsjsdlfjdfslksdfl",
            "fdskjsdfkljdskfklmdfjjjjjjjjjjjjjkmjfdslkfdslkjklfdjsjklfdsjkl" +
                    "dsqlkqsdqsdqslqsdjlsqdl"+
                    "dsjsdlfjdfslksdfl"
    };

    @Override
    public int getCount() {
        return slides_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_item_layout, container, false);

        ImageView imageView = (ImageView)view.findViewById(R.id.imgSlide);
        TextView textView = (TextView)view.findViewById(R.id.textViewSlideTitle);
        TextView textView1 = (TextView)view.findViewById(R.id.textViewSlideDesc);

        imageView.setImageResource(slides_images[position]);
        textView.setText(slides_heading[position]);
        textView1.setText(slide_desc[position]);

        container.addView(view);

        return view;


    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

            container.removeView((RelativeLayout)object);
    }
}
