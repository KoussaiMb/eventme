package app.projet.pfa.projetpfa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private EditText editTextE,editTextP;
    private Button btnC;
    private FirebaseAuth auth;
    private Button btnOublieMDP;
    private ProgressDialog mProgressDialog;
    private ProgressDialog progressDialog;
    private SessionManager session;
    private SQLiteHandler db;


    LoginButton loginButton;
    private CallbackManager callbackManager;
    private DatabaseReference databaseReference,firebaseDatabase;

    public static final String TAG1 = "MainActivity";
    public static final int RequestSignInCode = 7;
    private static final int RC_SIGN_IN = 234;
    GoogleSignInClient mGoogleSignInClient;

    public GoogleApiClient googleApiClient;
    Button SignOutButton;
    com.google.android.gms.common.SignInButton signInButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        auth = FirebaseAuth.getInstance();

        //signInButton = (com.google.android.gms.common.SignInButton)findViewById(R.id.login_with_google);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });


        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());




//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "your package name here",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }



        editTextE = (EditText)findViewById(R.id.emailCH);
        editTextP = (EditText)findViewById(R.id.passCH);
        btnC = (Button) findViewById(R.id.button_signin);
        btnOublieMDP = (Button) findViewById(R.id.btnOublieMDP);
        loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");

        databaseReference =  FirebaseDatabase.getInstance().getReference().child("Users");
        callbackManager = CallbackManager.Factory.create();


//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        // Print the user’s ID and the Auth Token to Android Studio’s Logcat Monitor//
//                        Log.d(TAG, "User ID: " +
//                                loginResult.getAccessToken().getUserId() + "\n" +
//                                "Auth Token: " + loginResult.getAccessToken().getToken());
//                    }
//
//                    // If the user cancels the login, then call onCancel//
//                    @Override
//                    public void onCancel() {
//                    }
//
//                    // If an error occurs, then call onError//
//                    @Override
//                    public void onError(FacebookException exception) {
//                    }
//                });
//


                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                System.out.println("onSuccess");

                ProgressDialog progressDialog = new ProgressDialog(SignInActivity.this);
                progressDialog.setMessage("Procesando datos...");
                progressDialog.show();
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        Bundle bFacebookData = getFacebookData(object);


                        final String device_token;

                      //  firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
                        device_token = FirebaseInstanceId.getInstance().getToken();






                        final String nameF =  bFacebookData.getString("first_name");
                        final String emailU =  bFacebookData.getString("email");
                        final String dateN =  bFacebookData.getString("birthday");
                        final String sexe =  bFacebookData.getString("gender");
                        final String location =  bFacebookData.getString("location");
                     //  final Users users = new Users(nameF, dateN, sexe,location,"pays","image","thumb_image","phone", emailU );


//                        auth.createUserWithEmailAndPassword(emailU,emailU).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                            @Override
//                            public void onComplete(@NonNull Task<AuthResult> task) {
//
//                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//
//
//
//                                String uid = user.getUid();
//
//                                firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
//
//                                firebaseDatabase.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//                                        if(task.isSuccessful())
//                                        {
//                                            Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
//                                            Intent intent = new Intent(SignInActivity.this, DashbordActivity.class);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                            startActivity(intent);
//                                            finish();
//                                        }
//                                    }
//                                });
//                            }
//                        });
//




                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
                Log.v("LoginActivity", "eruure");
            }
        });



//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                String userid = loginResult.getAccessToken().getUserId();
//
//                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse response) {
//                       // displayUserInfo(object);
//
//
//                    }
//                });
//
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "first_name, last_name, email, id");
//                graphRequest.setParameters(parameters);
//                graphRequest.executeAsync();
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//
//            }
//        });


        btnC.setOnClickListener(this);
        btnOublieMDP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextE.getText().toString().trim();

                if (email.isEmpty()) {
                    editTextE.setError("champ email est obligatoire");
                    editTextE.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    editTextE.setError("SVP entrez un email valide");
                    editTextE.requestFocus();
                    return;
                }

                auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(SignInActivity.this,"Verfier votre boite mail", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(SignInActivity.this, ""+task.getException(), Toast.LENGTH_LONG).show();

                        }
                    }
                });

            }
        });

    }

    private void UserSignInMethod() {

        Intent AuthIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);

        startActivityForResult(AuthIntent, RequestSignInCode);
    }

    private void signIn() {

        //getting the google signin intent
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();

        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }
            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));

            return bundle;
        }
        catch(JSONException e) {
            Log.d("tag","Error parsing JSON");
        }
        return null;
    }


//    private void displayUserInfo(JSONObject object) {
//
//        String first_name, last_name, email, id,location,nameLocation,dateNa,sexe;
//        first_name = "";
//        last_name ="";
//        email ="";
//        id ="";
//        location="";
//        nameLocation="";
//        dateNa="";
//        sexe="";
//        try{
//                first_name = object.getString("first_name");
//                last_name = object.getString("last_name");
//                email = object.getString("email");
//                id = object.getString("id");
//                sexe = object.getString("gender");
//                dateNa = object.getString("birthday");
//
//
//        }catch (JSONException e){
//            e.printStackTrace();
//        }
//
//        Toast.makeText(SignInActivity.this, first_name + last_name + email +id, Toast.LENGTH_LONG).show();
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case 7:
                GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

                if (googleSignInResult.isSuccess()){

                    GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();

                    FirebaseUserAuth(googleSignInAccount);
                }


                break;
            case 1:
                callbackManager.onActivityResult(requestCode, resultCode, data);

                break;
        }

        if (requestCode == RC_SIGN_IN) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(SignInActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }



    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Chargeemnt des données");
        mProgressDialog.setMessage("Vérification  en cours ");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            final FirebaseUser user = auth.getCurrentUser();



                            Thread thread = new Thread(){
                                @Override
                                public void run() {
                                    try {
                                        synchronized (this) {
                                            wait(5000);

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Bienvenue", Toast.LENGTH_LONG).show();


                                                    FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();

                                                    //  new CreateNewProduct().execute();

                                                    String uid = user1.getUid();

                                                    firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                                                   String device_token = FirebaseInstanceId.getInstance().getToken();

                                                    HashMap<String, String> hashMapUser = new HashMap<>();


                                                    String nom = user.getDisplayName();
                                                    String email = user.getEmail();
                                                    //String phone = user.getPhoneNumber();
                                                    String image = user.getPhotoUrl().toString();



                                                    Users users = new Users(nom,"Pas d info","Pas d info","ville", "Pas d info", image,image, "Pas d info", email, device_token);



                                                    firebaseDatabase.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful())
                                                            {

                                                                Thread thread = new Thread(){
                                                                    @Override
                                                                    public void run() {
                                                                        try {
                                                                            synchronized (this) {
                                                                                wait(5000);

                                                                                runOnUiThread(new Runnable() {
                                                                                    @Override
                                                                                    public void run() {
//                                                        Send send = new Send();
//                                                        send.execute();
                                                                                        Toast.makeText(getApplicationContext(), "utilisateur bien inscrit", Toast.LENGTH_LONG).show();
                                                                                        mProgressDialog.dismiss();
                                                                                        Intent intent = new Intent(SignInActivity.this, DashbordActivity.class);
                                                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                                        intent.putExtra("google","google");
                                                                                        startActivity(intent);
                                                                                        finish();

//
                                                                                    }
                                                                                });

                                                                            }
                                                                        } catch (InterruptedException e) {
                                                                            e.printStackTrace();
                                                                        }

                                                                    };
                                                                };
                                                                thread.start();


                                                            }
                                                        }
                                                    });















                                                    Log.i("displayName", user.getDisplayName().toString());
                                                    Log.i("emailUserSef", user.getEmail().toString());

//                                                    Intent intent = new Intent(SignInActivity.this, DashbordActivity.class);
//                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                    startActivity(intent);
//                                                    finish();

//
                                                }
                                            });

                                        }
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                };
                            };
                            thread.start();


















                        } else {
                            // If sign in fails, display a message to the user.
                            mProgressDialog.dismiss();
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    public void FirebaseUserAuth(GoogleSignInAccount googleSignInAccount) {

        AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);

        Toast.makeText(SignInActivity.this,""+ authCredential.getProvider(),Toast.LENGTH_LONG).show();

        auth.signInWithCredential(authCredential)
                .addOnCompleteListener(SignInActivity.this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task AuthResultTask) {

                        if (AuthResultTask.isSuccessful()){

                            // Getting Current Login user details.
                            FirebaseUser firebaseUser = auth.getCurrentUser();

                            // Showing Log out button.
//                            SignOutButton.setVisibility(View.VISIBLE);

                            // Hiding Login in button.
//                            signInButton.setVisibility(View.GONE);

                            // Showing the TextView.


                            // Setting up name into TextView.
                            Log.i("NAME =  ", firebaseUser.getDisplayName().toString());

                            // Setting up Email into TextView.
                            Log.i("Email =  ",  firebaseUser.getEmail().toString());

                        }else {
                            Toast.makeText(SignInActivity.this,"Something Went Wrong",Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    private void userLogin() {
        final String email = editTextE.getText().toString().trim();
        final String password = editTextP.getText().toString().trim();

        if (email.isEmpty()) {
            editTextE.setError("champ email est obligatoire");
            editTextE.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextE.setError("SVP entrez un email valide");
            editTextE.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            editTextP.setError("champ Mot de passe est obligatoire");
            editTextP.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextP.setError("Min 6 caracteres ou nombres");
            editTextP.requestFocus();
            return;
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Chargeemnt des données");
        mProgressDialog.setMessage("Vérification  en cours ");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String current_user_id = auth.getCurrentUser().getUid();
                    String deviceToken = FirebaseInstanceId.getInstance().getToken();

                    databaseReference.child(current_user_id).child("device_token").setValue(deviceToken).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {


                            Thread thread = new Thread(){
                                @Override
                                public void run() {
                                    try {
                                        synchronized (this) {
                                            wait(5000);

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Bienvenue", Toast.LENGTH_LONG).show();
                                                    mProgressDialog.dismiss();
                                                    if(email.equals("admin@gmail.com")&& password.equals("123456")){
                                                        Intent intent = new Intent(SignInActivity.this, AdminDashbord.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }else{
                                                        Intent intent = new Intent(SignInActivity.this, DashbordActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }


//
                                                }
                                            });

                                        }
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                };
                            };
                            thread.start();




                        }
                    });


                } else {
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    mProgressDialog.dismiss();
                }
            }
        });
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        if (auth.getCurrentUser() != null) {
//            finish();
//            startActivity(new Intent(this, DashbordActivity.class));
//        }
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.button_signin:
                userLogin();
                break;
        }
    }


        // Adding request to request queue

}