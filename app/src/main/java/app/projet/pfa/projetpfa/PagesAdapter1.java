package app.projet.pfa.projetpfa;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

class PagesAdapter1 extends FragmentPagerAdapter {
    public PagesAdapter1(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                EvenementOptionFragment evenementOptionFragment = new EvenementOptionFragment();
                return evenementOptionFragment;

            case 1:
                userOptionFragment userOptionFragment = new userOptionFragment();
                return userOptionFragment;

//            case 2:
//                ForumFragment forumFragment = new ForumFragment();
//                return forumFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:

                return "Les evenements";

            case 1:
                return "utilisateurs";


            default:
                return null;
        }
    }
}

