package app.projet.pfa.projetpfa;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

import static app.projet.pfa.projetpfa.InscritActivity.device_token;
import static app.projet.pfa.projetpfa.InscritActivity.passingEmailuser;

import java.util.Collections;
import java.util.Locale;


public class ProfileActivity extends AppCompatActivity {


    Dialog myDialog, myDialog1;

    private TextView textViewName, textViewPays, textViewage;

    private ProgressDialog progressDialog;


    private static final int GALLERY_PIK = 1;
    EditText editTextPays;

    private StorageReference mStorageRef;



    private DatabaseReference databaseReference, reference;
    private FirebaseUser firebaseUser;
    private ListView lv;
    private ImageView  imgProfile, imageViewD;
    private CircleImageView imageView;
    private FirebaseAuth auth;
    private CustomeAdapter customeAdapter;
    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{R.drawable.ic_email_black_24dp,
            R.drawable.ic_email_black_24dp,
            R.drawable.ic_email_black_24dp
            , R.drawable.ic_email_black_24dp,
            R.drawable.ic_phone_android_black_24dp,
            R.drawable.ic_group_add_black_24dp};
    private String[] myImageNameList = new String[]{"Benz", "Bike",
            "Car", "dsfds", "dlklkd", "dklkd"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        auth = FirebaseAuth.getInstance();

        lv = (ListView) findViewById(R.id.list);
        imgProfile = (ImageView) findViewById(R.id.updateProfile);
        imageViewD = (ImageView) findViewById(R.id.delete);
        imageView = (CircleImageView) findViewById(R.id.imgProfile);
        textViewName = (TextView) findViewById(R.id.tv_name);
        textViewPays = (TextView) findViewById(R.id.tv_address);
        textViewage = (TextView) findViewById(R.id.age);
        myDialog = new Dialog(this);

        mStorageRef = FirebaseStorage.getInstance().getReference();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        reference = FirebaseDatabase.getInstance().getReference().child("Users");


        String current_uid = firebaseUser.getUid();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        DatabaseReference reference = ref.child("Users").child(current_uid);


        final ArrayAdapter<String[]> stringArrayAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.lv_item);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // Users users = dataSnapshot.getValue(Users.class);


                String email = dataSnapshot.child("email").getValue().toString().trim();
                String phone = dataSnapshot.child("phone").getValue().toString().trim();
                String ville = dataSnapshot.child("ville").getValue().toString().trim();
                String sexe = dataSnapshot.child("sexe").getValue().toString().trim();
                String pays = dataSnapshot.child("pays").getValue().toString().trim();
//                String email="", phone= "", ville="", sexe="";
//                 email = users.getEmail().toString();
//                 phone = users.getPhone().toString();
//                 ville = users.getVille().toString();
//                 sexe = users.getSexe().toString();

                HashMap<String, String> map = new HashMap<>();
                map.put("pays",pays);
                map.put("sexe", sexe);
                map.put("phone", phone);
                map.put("email", email);

                final   ArrayList<HashMap<String, String>> hashMapArrayList  = new ArrayList<HashMap<String, String>>();

                hashMapArrayList.add(map);


                ListAdapter adapter = new SimpleAdapter(
                        ProfileActivity.this, hashMapArrayList,R.layout.lv_item,
                        new String[] {"email", "phone" ,"pays", "sexe"},
                        new int[] { R.id.name, R.id.name1,  R.id.name2, R.id.name4}
                );





                TextView textView = (TextView)findViewById(R.id.name);
                TextView textView1 = (TextView)findViewById(R.id.name1);
                TextView textView2 = (TextView)findViewById(R.id.name2);
                TextView textView4 = (TextView)findViewById(R.id.name4);

//                ImageView imageView = (ImageView) findViewById(R.id.imgView);
//               ImageView imageView1 = (ImageView) findViewById(R.id.imgView1);
//                ImageView imageView2 = (ImageView) findViewById(R.id.imgView2);
//                ImageView imageView4 = (ImageView) findViewById(R.id.imgView4);
//
//                imageView.setImageResource(R.drawable.ic_email_black_24dp);
//                imageView1.setImageResource(R.drawable.ic_phone_android_black_24dp);
//                imageView2.setImageResource(R.drawable.ic_location_city_black_24dp);
//                imageView4.setImageResource(R.drawable.ic_wc_black_24dp);








                lv.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        imageViewD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                suppUser();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setMessage("Vous voulez vraiment supprimer ?").setPositiveButton("Oui", dialogClickListener)
                        .setNegativeButton("Non", dialogClickListener).show();

            }
        });




//        FirebaseListAdapter<String> adapter = new FirebaseListAdapter<String>(
//                this,
//               String.class,
//                R.layout.lv_item,
//                reference)
//        {
//
//            @Override
//            protected void populateView(View v, String model, int position) {
//                ((ImageView)v.findViewById(R.id.imgView)).setImageResource(R.drawable.ic_email_black_24dp);
//                ((ImageView)v.findViewById(R.id.imgView1)).setImageResource(R.drawable.ic_phone_android_black_24dp);
//                ((ImageView)v.findViewById(R.id.imgView2)).setImageResource(R.drawable.ic_location_city_black_24dp);
//                ((ImageView)v.findViewById(R.id.imgView4)).setImageResource(R.drawable.ic_wc_black_24dp);
//               ((TextView)v.findViewById(R.id.name)).setText("encore pas d'info");
//                ((TextView)v.findViewById(R.id.name1)).setText("a");
//                ((TextView)v.findViewById(R.id.name2)).setText("b");
//                ((TextView)v.findViewById(R.id.name4)).setText(users.getSexe());
//
//            }
//        };
//
//        lv.setAdapter(adapter);


//
//        FirebaseListAdapter<Users> usersFirebaseListAdapter = new FirebaseListAdapter<Users>(this, Users.class, R.layout.lv_item, reference) {
//            @Override
//            protected void populateView(View v, Users users, int position) {
//
////                Object getemail = users.getEmail();
////                Object getsexe = users.getSexe();
////                Object getphone = users.getPhone();
////                Object getville = users.getVille();
////                Object o = "";
//
//                ((TextView)v.findViewById(R.id.name)).setText("dlsdksd");
////                ((TextView)v.findViewById(R.id.name1)).setText(users.getPhone());
////                ((TextView)v.findViewById(R.id.name2)).setText(users.getVille());
////                ((TextView)v.findViewById(R.id.name4)).setText(users.getSexe());
//
//
//
////
////                if(getemail.equals(o))
////                {
////                    ((TextView)v.findViewById(R.id.name)).setText("encore pas d'info");
////
////                }else {
////                    ((TextView)v.findViewById(R.id.name)).setText(users.getEmail());
////
////                }
////
////                if(getphone.toString().equals(o))
////                {
////                    ((TextView)v.findViewById(R.id.name1)).setText("encore pas d'info");
////
////                }else {
////                    ((TextView)v.findViewById(R.id.name1)).setText(users.getPhone());
////
////                }
////
////                if(getville.toString().equals(o))
////                {
////                    ((TextView)v.findViewById(R.id.name2)).setText("encore pas d'info");
////
////                }else {
////                    ((TextView)v.findViewById(R.id.name2)).setText(users.getVille());
////
////                }
////
////                if(getsexe.toString().equals(o))
////                {
////                    ((TextView)v.findViewById(R.id.name4)).setText("encore pas d'info");
////
////                }else {
////                    ((TextView)v.findViewById(R.id.name4)).setText(users.getSexe());
////
////                }
////
////                ((ImageView)v.findViewById(R.id.imgView)).setImageResource(R.drawable.ic_email_black_24dp);
////                ((ImageView)v.findViewById(R.id.imgView1)).setImageResource(R.drawable.ic_phone_android_black_24dp);
////                ((ImageView)v.findViewById(R.id.imgView2)).setImageResource(R.drawable.ic_location_city_black_24dp);
////                ((ImageView)v.findViewById(R.id.imgView4)).setImageResource(R.drawable.ic_wc_black_24dp);
//
//
//
//            }
//        };
//
//        lv.setAdapter(usersFirebaseListAdapter);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
        databaseReference.keepSynced(true);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String nom = "", ville = "", email, pays = "",  sexe = "", thumb_image = "", age = "", phone= "";

                String vide = "";
                String online ="";

                //    Users users = dataSnapshot.getValue(Users.class);




//                 nom = users.getNom().toString();
//                 email = users.getEmail().toString();
//                 phone = users.getPhone().toString();
//                 ville = users.getVille().toString();
//                 sexe = users.getSexe().toString();
//                 pays = users.getPays().toString();
//                 age = users.getAge().toString();
//               final String image = users.getImage().toString();
//                 thumb_image = users.getThumb_image().toString();




                nom = dataSnapshot.child("nom").getValue().toString().trim();
                textViewName.setText(nom);
                pays = dataSnapshot.child("pays").getValue().toString().trim();
                textViewPays.setText(pays);
                age = dataSnapshot.child("age").getValue().toString().trim();


                textViewage.setText(age + " ans");
                final String image = dataSnapshot.child("image").getValue().toString().trim();
                thumb_image = dataSnapshot.child("thumb_image").getValue().toString();

                if(!image.equals("default")){
                    Picasso.with(ProfileActivity.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                            .placeholder(R.drawable.user2).into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            Picasso.with(ProfileActivity.this).load(image).placeholder(R.drawable.user2).into(imageView);

                        }
                    });

                }



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openGallery = new Intent();
                openGallery.setType("image/*");
                openGallery.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(openGallery, "SELECT IMAGE"), GALLERY_PIK);

//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .start(ProfileActivity.this);

            }
        });


//        imageModelArrayList = populateList();
//        customeAdapter = new CustomeAdapter(this, imageModelArrayList);
//        lv.setAdapter(customeAdapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PIK && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setMinCropWindowSize(500, 500)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                final ProgressDialog progressDialog = new ProgressDialog(ProfileActivity.this);
                progressDialog.setTitle("Save image");
                progressDialog.setMessage("save image encours");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();


                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());
                String current_userUid = firebaseUser.getUid();


                Bitmap thumb_bitmap = new Compressor(this)
                        .setMaxWidth(200)
                        .setMaxHeight(200)
                        .setQuality(75)
                        .compressToBitmap(thumb_filePath);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] thumb_byte = baos.toByteArray();



                StorageReference storageReference = mStorageRef.child("profile_images").child(current_userUid+ ".jpg");
                final StorageReference thumb_filepath = mStorageRef.child("profile_images").child("thumbs").child(current_userUid+ ".jpg");


                storageReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        if(task.isSuccessful()){

                            @SuppressWarnings("VisibleForTesting")

                            final String donwalod_uri = task.getResult().getDownloadUrl().toString();

                            UploadTask uploadTask = thumb_filepath.putBytes(thumb_byte);
                            uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {

                                    String thumb_downloadUrl = thumb_task.getResult().getDownloadUrl().toString();

                                    if(thumb_task.isSuccessful()){

                                        Map update_hashMap = new HashMap();
                                        update_hashMap.put("image", donwalod_uri);
                                        update_hashMap.put("thumb_image", thumb_downloadUrl);

                                        databaseReference.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                if(task.isSuccessful()){

                                                    progressDialog.dismiss();
                                                    Toast.makeText(ProfileActivity.this, "Success Uploading.", Toast.LENGTH_LONG).show();

                                                }

                                            }
                                        });


                                    } else {

                                        Toast.makeText(ProfileActivity.this, "Error in uploading thumbnail.", Toast.LENGTH_LONG).show();
                                        progressDialog.dismiss();

                                    }


                                }
                            });



                        } else {

                            Toast.makeText(ProfileActivity.this, "Error in uploading.", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();

                        }

                    }
                });



            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();

            }
        }


    }


    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(20);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }


    public void testVide(String ch) {
        String pasInfo = "Pas d'informations";
        ch = pasInfo;


    }


    private ArrayList<ImageModel> populateList() {
        ArrayList<ImageModel> list = new ArrayList<>();
        for (int i = 0; i < myImageList.length; i++) {
            ImageModel imageModel = new ImageModel();
            imageModel.setName(myImageNameList[i]);
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }
        return list;
    }

    private void getAtt(EditText editText){

    }

    @SuppressLint("NewApi")
    String sexx;
    String tImage;
    String tTthumb_image;
    String Dimage;
    String Dthumb_image;
    public void ShowPopup(final View v) {
        TextView txtclose;
        Button btnFollow;
        final EditText editTextNom, editTextAge,editTextPhone;
        final Spinner spinner;
        final RadioGroup radioGroup;
        RadioButton radioButtonF,radioButtonH;








        myDialog.setContentView(R.layout.custompopup);

        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        editTextNom = (EditText) myDialog.findViewById(R.id.editTextNom);
        editTextAge = (EditText) myDialog.findViewById(R.id.editTextAge);
        editTextPays = (EditText) myDialog.findViewById(R.id.editTextPays);
        radioGroup = (RadioGroup) myDialog.findViewById(R.id.rg);
        spinner = (Spinner)myDialog.findViewById(R.id.spVille);
        editTextPhone = (EditText)myDialog.findViewById(R.id.phone);


        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);
        for (String country : countries) {
            System.out.println(country);
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        // set the view for the Drop down list
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // set the ArrayAdapter to the spinner
        spinner.setAdapter(dataAdapter);


        System.out.println("# countries found: " + countries.size());




//        ArrayList<String> arrayList = new ArrayList<String>();
//        Countries countries = new Countries();
//        countries.getPays(arrayList);
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,arrayList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        spinner.setAdapter(adapter);




        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioButtonF:
                        sexx ="Femme";
                        break;
                    case R.id.radioButtonH:
                        sexx ="Homme";
                        break;

                }
            }
        });




        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        String current_uid = firebaseUser.getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String nom = dataSnapshot.child("nom").getValue().toString().trim();
                String age = dataSnapshot.child("age").getValue().toString().trim();
                String pays = dataSnapshot.child("pays").getValue().toString().trim();
                String ville = dataSnapshot.child("ville").getValue().toString().trim();
                String phone = dataSnapshot.child("phone").getValue().toString().trim();
                String sexe = dataSnapshot.child("sexe").getValue().toString().trim();
                String email = dataSnapshot.child("email").getValue().toString().trim();
                String device_token = dataSnapshot.child("device_token").getValue().toString();

                editTextNom.setText(nom);
                editTextAge.setText(age);
                editTextPhone.setText(phone);


                String str = sexe;

                if(sexe.equals("Femme")){
                    radioGroup.check(R.id.radioButtonF);

                }else if (sexe.equals("Homme")){
                    radioGroup.check(R.id.radioButtonH);

                }

                Long d = Long.valueOf(ville);
                int x = safeLongToInt(d);
                spinner.setSelection(x);



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        txtclose.setText("X");
        btnFollow = (Button) myDialog.findViewById(R.id.btnfollow);



        DatabaseReference forTestingExistingImage = FirebaseDatabase.getInstance().getReference().child("Users");

        FirebaseAuth firebaseAuth;
        firebaseAuth= FirebaseAuth.getInstance();

        final String tt = firebaseAuth.getCurrentUser().getUid();


        forTestingExistingImage.child(tt).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                tImage = dataSnapshot.child("image").getValue().toString();
                tTthumb_image = dataSnapshot.child("thumb_image").getValue().toString();

                Log.d("tImage",tImage);
                if(tImage.equals("default"))

                {
                    Log.d("tImageFix",tImage);

                    Dimage = "default";
                    Dthumb_image = "default";
                }else{
                    Dimage = tImage;
                    Dthumb_image = tTthumb_image;
                }

                Log.d("finalImage   ",tImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //    Toast.makeText(ProfileActivity.this,passingEmailuser + " " + device_token, Toast.LENGTH_LONG).show();

                progressDialog = new ProgressDialog(v.getContext());

                progressDialog.setTitle("encrous");
                progressDialog.setMessage("save encours...");


                String n ="";
                n =  editTextNom.getText().toString().trim();
                String a ="";
                a = editTextAge.getText().toString().trim();
                String s ="";
                s = sexx;
                String p ="";
                p = editTextPhone.getText().toString().trim();
                String e ="";
                e = passingEmailuser;
                String pa ="";
                pa = editTextPays.getText().toString().trim();

                HashMap hashMapUser = new HashMap<>();
                hashMapUser.put("nom", n);
                hashMapUser.put("age", a);
                hashMapUser.put("sexe", s);
                hashMapUser.put("phone", p);
                hashMapUser.put("email",e);
                hashMapUser.put("pays", pa);
                hashMapUser.put("ville", "ville");
//                hashMapUser.put("image", "default");
//                hashMapUser.put("thumb_image", "default");
                hashMapUser.put("device_token",device_token );


                FirebaseAuth firebaseAuth;
                firebaseAuth= FirebaseAuth.getInstance();

                String tt = firebaseAuth.getCurrentUser().getUid();





                DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Users");



                Users users = new Users(n,a,s,"ville", pa, Dimage,Dthumb_image, p, passingEmailuser, device_token);
                reference.child(tt).setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){
                            progressDialog.dismiss();
                            myDialog.dismiss();
                        }else
                        {
                            Toast.makeText(ProfileActivity.this,"errure", Toast.LENGTH_LONG).show();
                        }
                    }
                });

//                databaseReference.setValue(hashMapUser).addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//
//                        if(task.isSuccessful())
//                            progressDialog.dismiss();
//                            myDialog.dismiss();
//
//                    }
//                });
//
            }
        });
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();

            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();


    }





    private void deleteUser(String uid){
        reference.child(uid).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }
            }
        });
    }

    private void suppUser() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String getUid = user.getUid();
        if (user != null) {

            user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if(task.isSuccessful()){


                        deleteUser(getUid);
                        Toast.makeText(ProfileActivity.this,"supprimer ", Toast.LENGTH_LONG).show();
                        sendToMain();

                    }else{
                        Toast.makeText(ProfileActivity.this,"errure " + task.getException(), Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    }

    private void sendToMain(){

        Intent intent  = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }

    private String getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }
}


//package app.projet.pfa.projetpfa;
//
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.net.Uri;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ListAdapter;
//import android.widget.ListView;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.SimpleAdapter;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.google.firebase.storage.FirebaseStorage;
//import com.google.firebase.storage.StorageReference;
//import com.google.firebase.storage.UploadTask;
//import com.squareup.picasso.Callback;
//import com.squareup.picasso.NetworkPolicy;
//import com.squareup.picasso.Picasso;
//import com.theartofdev.edmodo.cropper.CropImage;
//
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Random;
//import java.util.Set;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//import id.zelory.compressor.Compressor;
//
//import static app.projet.pfa.projetpfa.InscritActivity.device_token;
//import static app.projet.pfa.projetpfa.InscritActivity.passingEmailuser;
//
//
//public class ProfileActivity extends AppCompatActivity {
//
//
//    Dialog myDialog, myDialog1;
//
//    private TextView textViewName, textViewPays, textViewage;
//
//    private ProgressDialog progressDialog;
//
//
//    private static final int GALLERY_PIK = 1;
//    EditText editTextPays;
//
//    private StorageReference mStorageRef;
//
//
//
//    private DatabaseReference databaseReference;
//    private FirebaseUser firebaseUser;
//    private ListView lv;
//    private ImageView  imgProfile;
//    private CircleImageView imageView;
//    private FirebaseAuth auth;
//    private CustomeAdapter customeAdapter;
//    private ArrayList<ImageModel> imageModelArrayList;
//    private int[] myImageList = new int[]{R.drawable.ic_email_black_24dp,
//            R.drawable.ic_email_black_24dp,
//            R.drawable.ic_email_black_24dp
//            , R.drawable.ic_email_black_24dp,
//            R.drawable.ic_phone_android_black_24dp,
//            R.drawable.ic_group_add_black_24dp};
//    private String[] myImageNameList = new String[]{"Benz", "Bike",
//            "Car", "dsfds", "dlklkd", "dklkd"};
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_profile);
//
//        auth = FirebaseAuth.getInstance();
//
//        lv = (ListView) findViewById(R.id.list);
//        imgProfile = (ImageView) findViewById(R.id.updateProfile);
//        imageView = (CircleImageView) findViewById(R.id.imgProfile);
//        textViewName = (TextView) findViewById(R.id.tv_name);
//        textViewPays = (TextView) findViewById(R.id.tv_address);
//        textViewage = (TextView) findViewById(R.id.age);
//        myDialog = new Dialog(this);
//
//        mStorageRef = FirebaseStorage.getInstance().getReference();
//
//        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//
//
//
//        String current_uid = firebaseUser.getUid();
//
//        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
//
//        DatabaseReference reference = ref.child("Users").child(current_uid);
//
//
//        final ArrayAdapter<String[]> stringArrayAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.lv_item);
//
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                // Users users = dataSnapshot.getValue(Users.class);
//
//
//                String email = dataSnapshot.child("email").getValue().toString().trim();
//                String phone = dataSnapshot.child("phone").getValue().toString().trim();
//                String ville = dataSnapshot.child("ville").getValue().toString().trim();
//                String sexe = dataSnapshot.child("sexe").getValue().toString().trim();
////                String email="", phone= "", ville="", sexe="";
////                 email = users.getEmail().toString();
////                 phone = users.getPhone().toString();
////                 ville = users.getVille().toString();
////                 sexe = users.getSexe().toString();
//
//                HashMap<String, String> map = new HashMap<>();
//                map.put("ville",ville);
//                map.put("sexe", sexe);
//                map.put("phone", phone);
//                map.put("email", email);
//
//                final   ArrayList<HashMap<String, String>> hashMapArrayList  = new ArrayList<HashMap<String, String>>();
//
//                hashMapArrayList.add(map);
//
//
//                ListAdapter adapter = new SimpleAdapter(
//                        ProfileActivity.this, hashMapArrayList,R.layout.lv_item,
//                        new String[] {"email", "phone" ,"ville", "sexe"},
//                        new int[] { R.id.name, R.id.name1,  R.id.name2, R.id.name4}
//                );
//
//
//
//
//
//                TextView textView = (TextView)findViewById(R.id.name);
//                TextView textView1 = (TextView)findViewById(R.id.name1);
//                TextView textView2 = (TextView)findViewById(R.id.name2);
//                TextView textView4 = (TextView)findViewById(R.id.name4);
//
////                ImageView imageView = (ImageView) findViewById(R.id.imgView);
////               ImageView imageView1 = (ImageView) findViewById(R.id.imgView1);
////                ImageView imageView2 = (ImageView) findViewById(R.id.imgView2);
////                ImageView imageView4 = (ImageView) findViewById(R.id.imgView4);
////
////                imageView.setImageResource(R.drawable.ic_email_black_24dp);
////                imageView1.setImageResource(R.drawable.ic_phone_android_black_24dp);
////                imageView2.setImageResource(R.drawable.ic_location_city_black_24dp);
////                imageView4.setImageResource(R.drawable.ic_wc_black_24dp);
//
//
//
//
//
//
//
//
//                lv.setAdapter(adapter);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//
//
//
//
//
////        FirebaseListAdapter<String> adapter = new FirebaseListAdapter<String>(
////                this,
////               String.class,
////                R.layout.lv_item,
////                reference)
////        {
////
////            @Override
////            protected void populateView(View v, String model, int position) {
////                ((ImageView)v.findViewById(R.id.imgView)).setImageResource(R.drawable.ic_email_black_24dp);
////                ((ImageView)v.findViewById(R.id.imgView1)).setImageResource(R.drawable.ic_phone_android_black_24dp);
////                ((ImageView)v.findViewById(R.id.imgView2)).setImageResource(R.drawable.ic_location_city_black_24dp);
////                ((ImageView)v.findViewById(R.id.imgView4)).setImageResource(R.drawable.ic_wc_black_24dp);
////               ((TextView)v.findViewById(R.id.name)).setText("encore pas d'info");
////                ((TextView)v.findViewById(R.id.name1)).setText("a");
////                ((TextView)v.findViewById(R.id.name2)).setText("b");
////                ((TextView)v.findViewById(R.id.name4)).setText(users.getSexe());
////
////            }
////        };
////
////        lv.setAdapter(adapter);
//
//
////
////        FirebaseListAdapter<Users> usersFirebaseListAdapter = new FirebaseListAdapter<Users>(this, Users.class, R.layout.lv_item, reference) {
////            @Override
////            protected void populateView(View v, Users users, int position) {
////
//////                Object getemail = users.getEmail();
//////                Object getsexe = users.getSexe();
//////                Object getphone = users.getPhone();
//////                Object getville = users.getVille();
//////                Object o = "";
////
////                ((TextView)v.findViewById(R.id.name)).setText("dlsdksd");
//////                ((TextView)v.findViewById(R.id.name1)).setText(users.getPhone());
//////                ((TextView)v.findViewById(R.id.name2)).setText(users.getVille());
//////                ((TextView)v.findViewById(R.id.name4)).setText(users.getSexe());
////
////
////
//////
//////                if(getemail.equals(o))
//////                {
//////                    ((TextView)v.findViewById(R.id.name)).setText("encore pas d'info");
//////
//////                }else {
//////                    ((TextView)v.findViewById(R.id.name)).setText(users.getEmail());
//////
//////                }
//////
//////                if(getphone.toString().equals(o))
//////                {
//////                    ((TextView)v.findViewById(R.id.name1)).setText("encore pas d'info");
//////
//////                }else {
//////                    ((TextView)v.findViewById(R.id.name1)).setText(users.getPhone());
//////
//////                }
//////
//////                if(getville.toString().equals(o))
//////                {
//////                    ((TextView)v.findViewById(R.id.name2)).setText("encore pas d'info");
//////
//////                }else {
//////                    ((TextView)v.findViewById(R.id.name2)).setText(users.getVille());
//////
//////                }
//////
//////                if(getsexe.toString().equals(o))
//////                {
//////                    ((TextView)v.findViewById(R.id.name4)).setText("encore pas d'info");
//////
//////                }else {
//////                    ((TextView)v.findViewById(R.id.name4)).setText(users.getSexe());
//////
//////                }
//////
//////                ((ImageView)v.findViewById(R.id.imgView)).setImageResource(R.drawable.ic_email_black_24dp);
//////                ((ImageView)v.findViewById(R.id.imgView1)).setImageResource(R.drawable.ic_phone_android_black_24dp);
//////                ((ImageView)v.findViewById(R.id.imgView2)).setImageResource(R.drawable.ic_location_city_black_24dp);
//////                ((ImageView)v.findViewById(R.id.imgView4)).setImageResource(R.drawable.ic_wc_black_24dp);
////
////
////
////            }
////        };
////
////        lv.setAdapter(usersFirebaseListAdapter);
//
//        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
//        databaseReference.keepSynced(true);
//
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                String nom = "", ville = "", email, pays = "",  sexe = "", thumb_image = "", age = "", phone= "";
//
//                String vide = "";
//                String online ="";
//
//                //    Users users = dataSnapshot.getValue(Users.class);
//
//
//
//
////                 nom = users.getNom().toString();
////                 email = users.getEmail().toString();
////                 phone = users.getPhone().toString();
////                 ville = users.getVille().toString();
////                 sexe = users.getSexe().toString();
////                 pays = users.getPays().toString();
////                 age = users.getAge().toString();
////               final String image = users.getImage().toString();
////                 thumb_image = users.getThumb_image().toString();
//
//
//
//
//                nom = dataSnapshot.child("nom").getValue().toString().trim();
//                textViewName.setText(nom);
//                pays = dataSnapshot.child("pays").getValue().toString().trim();
//                textViewPays.setText(pays);
//                age = dataSnapshot.child("age").getValue().toString().trim();
//                textViewage.setText(age + " ans");
//                final String image = dataSnapshot.child("image").getValue().toString().trim();
//                thumb_image = dataSnapshot.child("thumb_image").getValue().toString();
//
//                if(!image.equals("default")){
//                    Picasso.with(ProfileActivity.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
//                            .placeholder(R.drawable.user2).into(imageView, new Callback() {
//                        @Override
//                        public void onSuccess() {
//
//                        }
//
//                        @Override
//                        public void onError() {
//
//                            Picasso.with(ProfileActivity.this).load(image).placeholder(R.drawable.user2).into(imageView);
//
//                        }
//                    });
//
//                }
//
//
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent openGallery = new Intent();
//                openGallery.setType("image/*");
//                openGallery.setAction(Intent.ACTION_GET_CONTENT);
//
//                startActivityForResult(Intent.createChooser(openGallery, "SELECT IMAGE"), GALLERY_PIK);
//
////                CropImage.activity()
////                        .setGuidelines(CropImageView.Guidelines.ON)
////                        .start(ProfileActivity.this);
//
//            }
//        });
//
//
////        imageModelArrayList = populateList();
////        customeAdapter = new CustomeAdapter(this, imageModelArrayList);
////        lv.setAdapter(customeAdapter);
//
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == GALLERY_PIK && resultCode == RESULT_OK) {
//
//            Uri imageUri = data.getData();
//
//            CropImage.activity(imageUri)
//                    .setAspectRatio(1, 1)
//                    .setMinCropWindowSize(500, 500)
//                    .start(this);
//        }
//
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//
//            if (resultCode == RESULT_OK) {
//
//                final ProgressDialog progressDialog = new ProgressDialog(ProfileActivity.this);
//                progressDialog.setTitle("Save image");
//                progressDialog.setMessage("save image encours");
//                progressDialog.setCanceledOnTouchOutside(false);
//                progressDialog.show();
//
//
//                Uri resultUri = result.getUri();
//
//                File thumb_filePath = new File(resultUri.getPath());
//                String current_userUid = firebaseUser.getUid();
//
//
//                Bitmap thumb_bitmap = new Compressor(this)
//                        .setMaxWidth(200)
//                        .setMaxHeight(200)
//                        .setQuality(75)
//                        .compressToBitmap(thumb_filePath);
//
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//                final byte[] thumb_byte = baos.toByteArray();
//
//
//
//                StorageReference storageReference = mStorageRef.child("profile_images").child(current_userUid+ ".jpg");
//                final StorageReference thumb_filepath = mStorageRef.child("profile_images").child("thumbs").child(current_userUid+ ".jpg");
//
//
//                storageReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
//
//                        if(task.isSuccessful()){
//
//                            @SuppressWarnings("VisibleForTesting")
//
//                            final String donwalod_uri = task.getResult().getDownloadUrl().toString();
//
//                            UploadTask uploadTask = thumb_filepath.putBytes(thumb_byte);
//                            uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//                                @Override
//                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {
//
//                                    String thumb_downloadUrl = thumb_task.getResult().getDownloadUrl().toString();
//
//                                    if(thumb_task.isSuccessful()){
//
//                                        Map update_hashMap = new HashMap();
//                                        update_hashMap.put("image", donwalod_uri);
//                                        update_hashMap.put("thumb_image", thumb_downloadUrl);
//
//                                        databaseReference.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                            @Override
//                                            public void onComplete(@NonNull Task<Void> task) {
//
//                                                if(task.isSuccessful()){
//
//                                                    progressDialog.dismiss();
//                                                    Toast.makeText(ProfileActivity.this, "Success Uploading.", Toast.LENGTH_LONG).show();
//
//                                                }
//
//                                            }
//                                        });
//
//
//                                    } else {
//
//                                        Toast.makeText(ProfileActivity.this, "Error in uploading thumbnail.", Toast.LENGTH_LONG).show();
//                                        progressDialog.dismiss();
//
//                                    }
//
//
//                                }
//                            });
//
//
//
//                        } else {
//
//                            Toast.makeText(ProfileActivity.this, "Error in uploading.", Toast.LENGTH_LONG).show();
//                            progressDialog.dismiss();
//
//                        }
//
//                    }
//                });
//
//
//
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//
//                Exception error = result.getError();
//
//            }
//        }
//
//
//    }
//
//
//    public static String random() {
//        Random generator = new Random();
//        StringBuilder randomStringBuilder = new StringBuilder();
//        int randomLength = generator.nextInt(20);
//        char tempChar;
//        for (int i = 0; i < randomLength; i++){
//            tempChar = (char) (generator.nextInt(96) + 32);
//            randomStringBuilder.append(tempChar);
//        }
//        return randomStringBuilder.toString();
//    }
//
//
//    public void testVide(String ch) {
//        String pasInfo = "Pas d'informations";
//        ch = pasInfo;
//
//
//    }
//
//
//    private ArrayList<ImageModel> populateList() {
//        ArrayList<ImageModel> list = new ArrayList<>();
//        for (int i = 0; i < myImageList.length; i++) {
//            ImageModel imageModel = new ImageModel();
//            imageModel.setName(myImageNameList[i]);
//            imageModel.setImage_drawable(myImageList[i]);
//            list.add(imageModel);
//        }
//        return list;
//    }
//
//    private void getAtt(EditText editText){
//
//    }
//
//    String sexx;
//    String tImage;
//    String tTthumb_image;
//    String Dimage;
//    String Dthumb_image;
//    public void ShowPopup(View v) {
//        TextView txtclose;
//        Button btnFollow;
//        final EditText editTextNom, editTextAge,editTextPhone;
//        Spinner spinner;
//        RadioGroup radioGroup;
//        RadioButton radioButtonF,radioButtonH;
//
//
//        myDialog.setContentView(R.layout.custompopup);
//
//        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
//        editTextNom = (EditText) myDialog.findViewById(R.id.editTextNom);
//        editTextAge = (EditText) myDialog.findViewById(R.id.editTextAge);
//        editTextPays = (EditText) myDialog.findViewById(R.id.editTextPays);
//        radioGroup = (RadioGroup) myDialog.findViewById(R.id.rg);
//        spinner = (Spinner)myDialog.findViewById(R.id.spVille);
//        editTextPhone = (EditText)myDialog.findViewById(R.id.phone);
//
//        ArrayList<String> arrayList = new ArrayList<String>();
//        Countries countries = new Countries();
//        countries.getPays(arrayList);
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,arrayList);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        spinner.setAdapter(adapter);
//
//
//
//
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId){
//                    case R.id.radioButtonF:
//                        sexx ="Femme";
//                        break;
//                    case R.id.radioButtonH:
//                        sexx ="Homme";
//                        break;
//
//                }
//            }
//        });
//
//
//
//
//        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//
//        String current_uid = firebaseUser.getUid();
//
//        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
//
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                String nom = dataSnapshot.child("nom").getValue().toString().trim();
//                String age = dataSnapshot.child("age").getValue().toString().trim();
//                String pays = dataSnapshot.child("pays").getValue().toString().trim();
//                String phone = dataSnapshot.child("phone").getValue().toString().trim();
//                String sexe = dataSnapshot.child("sexe").getValue().toString().trim();
//                String email = dataSnapshot.child("email").getValue().toString().trim();
//                String device_token = dataSnapshot.child("device_token").getValue().toString();
//
//                editTextNom.setText(nom);
//                editTextAge.setText(age);
//                editTextPhone.setText(phone);
//
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//        txtclose.setText("X");
//        btnFollow = (Button) myDialog.findViewById(R.id.btnfollow);
//
//
//
//        DatabaseReference forTestingExistingImage = FirebaseDatabase.getInstance().getReference().child("Users");
//
//        FirebaseAuth firebaseAuth;
//        firebaseAuth= FirebaseAuth.getInstance();
//
//        final String tt = firebaseAuth.getCurrentUser().getUid();
//
//
//        forTestingExistingImage.child(tt).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                tImage = dataSnapshot.child("image").getValue().toString();
//                tTthumb_image = dataSnapshot.child("thumb_image").getValue().toString();
//
//                Log.d("tImage",tImage);
//                if(tImage.equals("default"))
//
//                {
//                    Log.d("tImageFix",tImage);
//
//                    Dimage = "default";
//                    Dthumb_image = "default";
//                }else{
//                    Dimage = tImage;
//                    Dthumb_image = tTthumb_image;
//                }
//
//                Log.d("finalImage   ",tImage);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//
//
//
//        btnFollow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                //    Toast.makeText(ProfileActivity.this,passingEmailuser + " " + device_token, Toast.LENGTH_LONG).show();
//
//                progressDialog = new ProgressDialog(v.getContext());
//
//                progressDialog.setTitle("encrous");
//                progressDialog.setMessage("save encours...");
//
//
//                String n ="";
//                n =  editTextNom.getText().toString().trim();
//                String a ="";
//                a = editTextAge.getText().toString().trim();
//                String s ="";
//                s = sexx;
//                String p ="";
//                p = editTextPhone.getText().toString().trim();
//                String e ="";
//                e = passingEmailuser;
//                String pa ="";
//                pa = editTextPays.getText().toString().trim();
//
//                HashMap hashMapUser = new HashMap<>();
//                hashMapUser.put("nom", n);
//                hashMapUser.put("age", a);
//                hashMapUser.put("sexe", s);
//                hashMapUser.put("phone", p);
//                hashMapUser.put("email",e);
//                hashMapUser.put("pays", pa);
//                hashMapUser.put("ville", "ville");
////                hashMapUser.put("image", "default");
////                hashMapUser.put("thumb_image", "default");
//                hashMapUser.put("device_token",device_token );
//
//
//                FirebaseAuth firebaseAuth;
//                firebaseAuth= FirebaseAuth.getInstance();
//
//                String tt = firebaseAuth.getCurrentUser().getUid();
//
//
//
//
//
//                DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Users");
//
//
//
//                Users users = new Users(n,a,s,"ville", pa, Dimage,Dthumb_image, p, passingEmailuser, device_token);
//                reference.child(tt).setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//
//                        if(task.isSuccessful()){
//                            progressDialog.dismiss();
//                            myDialog.dismiss();
//                        }else
//                        {
//                            Toast.makeText(ProfileActivity.this,"errure", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//
////                databaseReference.setValue(hashMapUser).addOnCompleteListener(new OnCompleteListener<Void>() {
////                    @Override
////                    public void onComplete(@NonNull Task<Void> task) {
////
////                        if(task.isSuccessful())
////                            progressDialog.dismiss();
////                            myDialog.dismiss();
////
////                    }
////                });
////
//            }
//        });
//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//
//            }
//        });
//
//        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        myDialog.show();
//
//
//    }
//
//
//}
//
//
