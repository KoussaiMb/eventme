package app.projet.pfa.projetpfa;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.google.android.gms.location.LocationListener;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class serachEventActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    private DatabaseReference mUsersDatabase, databaseReferenceE, databaseReference;
    FusedLocationProviderClient mFusedLocationClient;


    private LinearLayoutManager mLayoutManager;

    MaterialSearchView materialSearchView;
    private static double latiCurrent, langiCurrent;
    private static double latitude, longitude;
    ArrayList<String> arrayList = new ArrayList<String>();

    private List<Evenement> movieList = new ArrayList<>();
    private EvenetAdapter mAdapter;





    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serach_event);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(serachEventActivity.this);


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(serachEventActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            latiCurrent = location.getLatitude();
                            langiCurrent = location.getLongitude();

                            Log.d("latiCurrent", String.valueOf(latiCurrent));
                            Log.d("langiCurrent", String.valueOf(langiCurrent));


                        }
                    }
                });


        mToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(mToolbar);


        getSupportActionBar().setTitle("Retrouvez des evenements");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        materialSearchView = (MaterialSearchView)findViewById(R.id.mysearchEvent);
        materialSearchView.clearFocus();

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Evenement");

        databaseReferenceE = FirebaseDatabase.getInstance().getReference().child("Evenement");

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Evenement");

        databaseReferenceE.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final HashMap<String, GetEventLocation> hashMapUser = new HashMap<String, GetEventLocation>();
//
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    final GetEventLocation getEventLocation = new GetEventLocation();
                    getEventLocation.setId(postSnapshot.getKey());

                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Evenement").child(getEventLocation.getId().toString());

                    databaseReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String lat = dataSnapshot.child("lat").getValue().toString();
                            String lang = dataSnapshot.child("lang").getValue().toString();
                            getEventLocation.setLat(lat);
                            getEventLocation.setLang(lang);

//                            hashMapUser.put("keymap",getEventLocation);

                            LatLng latLngStart, latLngEnd;

                            latitude = Double.parseDouble(lat);
                            longitude = Double.parseDouble(lang);


                            latLngStart = new LatLng(latiCurrent, langiCurrent);
                            latLngEnd   = new LatLng(latitude, longitude);


//                          double x =  CalculationByDistance(latLngStart, latLngEnd);
//
//                            Log.v("xx",String.valueOf(x));

                            Location locationE = new Location("");
                            locationE.setLatitude(latitude);
                            locationE.setLongitude(longitude);

                            Location locationS = new Location("");
                            locationS.setLatitude(latiCurrent);
                            locationS.setLongitude(langiCurrent);

                            double distance= (int) locationS.distanceTo(locationE);
                            Log.d("distance", String.valueOf(distance));

                            double distanceKilo = distanceKilo(distance);

                            if(distanceKilo < 400 )
                            {

                                arrayList.add(getEventLocation.getId());

                            }







                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }




                // String id = dataSnapshot.getValue().toString();

//                String lat = dataSnapshot.child(id).child("lat").getValue().toString();
//                String lang = dataSnapshot.child(id).child("lang").getValue().toString();
//
//                getEventLocation.setId(id);
//                getEventLocation.setLat(lat);
//                getEventLocation.setLang(lang);
//
//                HashMap<String, GetEventLocation> hashMapUser = new HashMap<String, GetEventLocation>();
//                hashMapUser.put(id,getEventLocation);
//
//                Log.v("map",hashMapUser.toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mLayoutManager = new LinearLayoutManager(this);

        mUsersList = (RecyclerView) findViewById(R.id.rcvSerach);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(mLayoutManager);



        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // firebaseUserSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                firebaseUserSearch(newText);
                return false;
            }
        });
    }


    public void a()
    {
        for (int i=0;i<arrayList.size(); i++){
            Log.d("idSelect", arrayList.get(i));
            //  firebaseUserSearchDateByProximite("-LB2_KcMT0P1L_EQtCnx");

        }
    }

    private double distanceKilo(double meter) {

        double to;

        to =  meter/1000;

        return to;
    }



    private void firebaseUserSearchDate() {

        Query firebaseSearchQuery = mUsersDatabase.orderByChild("dateDeb");

        FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                serachEventActivity.UsersHolder.class,
                firebaseSearchQuery
        ) {
            @Override
            protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());


                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, serachEventActivity.this);


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, serachEventActivity.this);


                }

                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(serachEventActivity.this, viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }

    private void firebaseUserSearchDateByProximiteTest() {


        for (int i=0;i<arrayList.size(); i++) {
            // Log.d("idSelect", arrayList.get(i));
            //  firebaseUserSearchDateByProximite("-LB2_KcMT0P1L_EQtCnx");


            databaseReference.child(arrayList.get(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    Evenement evenement;
                    String title = dataSnapshot.child("title").getValue().toString();
                    String lieu = dataSnapshot.child("lieu").getValue().toString();
                    String dateDeb = dataSnapshot.child("dateDeb").getValue().toString();

                    evenement = new Evenement(title, lieu, dateDeb);

                    mAdapter = new EvenetAdapter(movieList);


                    movieList.add(evenement);


                    mUsersList.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();






                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }


    private void firebaseUserSearchDateByProximite() {

        databaseReference.child("-LB2_KcMT0P1L_EQtCnx").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d("title", dataSnapshot.child("title").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                serachEventActivity.UsersHolder.class,
                databaseReference
        ) {
            @Override
            protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());


                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, serachEventActivity.this);


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, serachEventActivity.this);


                }
//
                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(serachEventActivity.this, viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }

    private void firebaseUserSearch(String searchText) {

        Query firebaseSearchQuery = mUsersDatabase.orderByChild("title").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                serachEventActivity.UsersHolder.class,
                firebaseSearchQuery
        ) {
            @Override
            protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());



                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, serachEventActivity.this);


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, serachEventActivity.this);


                }

                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(serachEventActivity.this, viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }

    private void firebaseUserSearchByCategorie(String cirtere ,String searchText) {

        Query firebaseSearchQuery = mUsersDatabase.orderByChild(cirtere).startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                serachEventActivity.UsersHolder.class,
                firebaseSearchQuery
        ) {
            @Override
            protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());


                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, serachEventActivity.this);


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, serachEventActivity.this);


                }

                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(serachEventActivity.this, viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, serachEventActivity.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                serachEventActivity.UsersHolder.class,
                mUsersDatabase
        ) {
            @Override
            protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());

                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, serachEventActivity.this);


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, serachEventActivity.this);


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, serachEventActivity.this);


                }



                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(serachEventActivity.this, viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        mUsersList.setAdapter(firebaseRecyclerAdapter);
    }

//    @Override
//    public void onLocationChanged(Location location) {
//        myCurrentLatitude = (double) (location.getLatitude());
//        myCurrentLongitude = (double) (location.getLongitude());
//
//        Log.d("lat", String.valueOf(myCurrentLatitude));
//        Log.d("lang", String.valueOf(myCurrentLongitude));
//
//
//    }

    public static class UsersHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setLieu(String date) {

            TextView textView = (TextView) mView.findViewById(R.id.textView3);
            textView.setText(date);
        }

        public void setTitle(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.textView2);
            textView.setText(name);
        }

        public void setDate(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.textView4);
            textView.setText(name);
        }

        public void setUserImage(int thumb_image, Context ctx) {

            ImageView userImageView = (ImageView) mView.findViewById(R.id.imageView4);
            userImageView.setImageResource(thumb_image);


        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_serach_event, menu);

        MenuItem item = menu.findItem(R.id.searchEvent);
        materialSearchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int x = item.getItemId();

        if(x == R.id.searchOption)
            diplayBoiteDialog();

        if(x == R.id.searchCategorie)
            diplayBoiteDialogOptionCategorieSearch();



        return true;
    }

    private void diplayBoiteDialogOptionCategorieSearch() {
        CharSequence options[] = new CharSequence[]{"Musique", "Sport", "Culture", "Rencontre", "Autre"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(serachEventActivity.this);

        builder.setTitle("Choisir la catégorie\"");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                String [] tab = {"Musique", "Sport", "Culture", "Rencontre", "Autre"};
                String cirtere = "categorie";

                if(i == 0){

                    firebaseUserSearchByCategorie(cirtere, tab[0]);


                }

                if(i == 1){

                    firebaseUserSearchByCategorie(cirtere, tab[1]);

                }
                if(i == 2){

                    firebaseUserSearchByCategorie(cirtere, tab[2]);

                }
                if(i == 3){

                    firebaseUserSearchByCategorie(cirtere, tab[3]);

                }

                if(i == 4){

                    firebaseUserSearchByCategorie(cirtere, tab[4]);

                }


            }
        });
        builder.show();


    }

    private void diplayBoiteDialog() {
        CharSequence options[] = new CharSequence[]{"Evenement a proximté", "Afficher par date"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(serachEventActivity.this);

        builder.setTitle("Select Options");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                if(i == 0){

                    firebaseUserSearchDateByProximiteTest();
                }

                if(i == 1){

                    firebaseUserSearchDate();

                }


            }
        });
        builder.show();

    }



    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 300;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }



}
