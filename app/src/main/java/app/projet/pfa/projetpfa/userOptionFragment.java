package app.projet.pfa.projetpfa;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static app.projet.pfa.projetpfa.DashbordActivity.option;
import static com.facebook.FacebookSdk.getApplicationContext;


public class userOptionFragment extends Fragment {

    private View mainView;
    private static String getInetentFormIndex = "";
    private RecyclerView recyclerViewFriend;
    private DatabaseReference databaseReference, databaseReferenceUsers,databaseReferenceKey;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReferenceEvenet;
    private String current_id_user;

    public userOptionFragment() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_user_option, container, false);


        recyclerViewFriend = (RecyclerView) mainView.findViewById(R.id.listUserOptionFragment);
        firebaseAuth = FirebaseAuth.getInstance();
        current_id_user = firebaseAuth.getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceKey = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReference.keepSynced(true);
        databaseReferenceKey.keepSynced(true);
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReferenceUsers.keepSynced(true);
        recyclerViewFriend.setHasFixedSize(true);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(getContext()));




        return mainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Users, usersActivity.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, usersActivity.UsersViewHolder>(

                Users.class,
                R.layout.users_single_layout,
                usersActivity.UsersViewHolder.class,
                databaseReferenceUsers

        ) {
            @Override
            protected void populateViewHolder(usersActivity.UsersViewHolder usersViewHolder, Users users, int position) {

                usersViewHolder.setDisplayName(users.getNom());
                usersViewHolder.setUserStatus(users.getPays());
                usersViewHolder.setUserImage(users.getThumb_image(), getApplicationContext());

                final String user_id = getRef(position).getKey();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                final String curenUserUid = user.getUid();

                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(user_id.equals(curenUserUid))
                        {
                            startActivity(new Intent(getActivity(),ProfileActivity.class));
                        }
                        else{
                            Intent profileIntent = new Intent(getActivity(), userProfilVisiteActivity.class);
                            profileIntent.putExtra("user_id", user_id);
                            startActivity(profileIntent);
                        }



                    }
                });

            }
        };


        recyclerViewFriend.setAdapter(firebaseRecyclerAdapter);

    }


    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayName(String name){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);

        }

        public void setUserStatus(String status){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(status);


        }

        public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user2).into(userImageView);

        }


    }

}


