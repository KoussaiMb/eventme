package app.projet.pfa.projetpfa;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.googlecode.mp4parser.authoring.Track;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;


public class EventActivity extends AppCompatActivity implements   GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,  View.OnClickListener {

    static String tes, eventKey, testEventID;
    private DatabaseReference mDatabase, databaseReference;
    Button btnDatePicker, btnTimePicker;
    EditText txtDate, txtTime,input_title;
    protected static String getPrixFormEvent;
    private StorageReference mStorageRef;
    RadioGroup rgType;
    String type="";
    String ServerURL = "http://192.168.1.100/pfa/getEvenet.php" ;
    private FirebaseUser firebaseUser;
    static String radioData, uid, getSpinerItem ;
    private static final int GALLERY_PIK1 = 1;
    MultiAutoCompleteTextView multiAutoCompleteTextView;
    private FirebaseAuth auth;
    List<Evenement> evenements;
    Button btnSaveData;
    TextView dateDebut,dateFin,TimeDeb,TimeFin;
    TextInputLayout textInputLayoutDateDeb;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private static final String TAG = "EventActivity";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private AutoCompleteTextView mAutocompleteTextView;
    private GoogleApiClient mGoogleApiClient;
    protected static String ifLieuNoUpdate, ifLatNoUpdate, ifLangNoUpdate, ifKeyNoUpdate  ;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    String radioBtn="";
    private static String lang;
    private static String lat;
    private static String keyIsWebSite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Ajouter un evenement ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAutocompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        mAutocompleteTextView.setThreshold(3);
        input_title = (EditText)findViewById(R.id.input_title);
        dateDebut= (TextView)findViewById(R.id.in_dated);
        TimeDeb= (TextView)findViewById(R.id.btn_time);
        dateFin=(TextView) findViewById(R.id.dateFin);
        TimeFin=(TextView) findViewById(R.id.TimeFin);
        btnSaveData = (Button) findViewById(R.id.btnSaveData);
        rgType = (RadioGroup)findViewById(R.id.rgType);
        multiAutoCompleteTextView = (MultiAutoCompleteTextView)findViewById(R.id.multiAutoCompleteTextView);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        dateDebut.setOnClickListener(this);
        TimeDeb.setOnClickListener(this);
        dateFin.setOnClickListener(this);
        TimeFin.setOnClickListener(this);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("EvenementUsers");





        final String current_uid = firebaseUser.getUid();
        auth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        List<String> categories = new ArrayList<String>();
        categories.add("Musique");
        categories.add("Rencontre");
        categories.add("Sport");
        categories.add("Autre");





        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);
        spinner.setPrompt("Choisir la categorie ");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getSpinerItem = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

       // getSpinerItem = parent.getItemAtPosition(position).toString();


        uid= user.getUid();
        mGoogleApiClient = new GoogleApiClient.Builder(EventActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);


        mDatabase = FirebaseDatabase.getInstance().getReference("Evenement");

//
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Ajouter des images", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//
////                        if(!eventKey.equals("") && !eventKey.isEmpty()&& eventKey != null)
////                        {
////                            Intent intent = new Intent(EventActivity.this, MultipleImage.class);
////                            intent.putExtra("eventKey", eventKey);
////                            startActivity(intent);
////                        }else {
////                            Toast.makeText(EventActivity.this, "il faut remplir d'abord", Toast.LENGTH_LONG).show();
////                        }
//
////                Intent intent = new Intent(EventActivity.this, MultipleImage.class);
//
//
//
//            }
//        });





        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if(checkedId == R.id.radioButton2)
                    {
                        showChangeLangDialog();
                    }
            }
        });





        btnSaveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedRadioButtonID = rgType.getCheckedRadioButtonId();

                if(selectedRadioButtonID == -1)
                {
                    Toast.makeText(EventActivity.this,"champs obligatoire", Toast.LENGTH_LONG).show();
                    return;

                }
                else {
                    RadioButton selectedRadioButton = (RadioButton) findViewById(selectedRadioButtonID);
                    type = selectedRadioButton.getText().toString();


                }




                    final String mTitle = input_title.getText().toString().trim();
                    final String mLieu  = mAutocompleteTextView.getText().toString() ;
                    final String mDateDeb = dateDebut.getText().toString();
                    String mHeureDef = TimeFin.getText().toString();
                    final String mDateFin = dateFin.getText().toString();
                    String mHeureDeb = TimeDeb.getText().toString();
                    String mDesciption = multiAutoCompleteTextView.getText().toString();
                    final String cat = getSpinerItem;
                    String ttype = type.toString();


                Log.v("tye",type);

                    Evenement evenement = new Evenement();

                    evenement.setTitle(mTitle);
                    evenement.setDateDeb(mDateDeb);
                    evenement.setDateFin(mDateFin);
                    evenement.setHeureDeb(mHeureDeb);
                    evenement.setHeureFin(mHeureDef);
                    evenement.setLieu(mLieu);
                    evenement.setType(ttype);
                    evenement.setDescription(mDesciption);
                    evenement.setUserCr(uid);
                    evenement.setCategorie(cat);
                    evenement.setImage("image");
                    evenement.setThumb_image("thumb_image");
                    evenement.setLang(lang);
                    evenement.setLat(lat);
                    evenement.setKey(keyIsWebSite);
                    ifLieuNoUpdate = mLieu;

                mDatabase.push().setValue(evenement) .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        if(task.isSuccessful()){

                            InsertData(mTitle, mLieu, mDateDeb, mDateFin, cat);

                          final String eventKey =   mDatabase.push().getKey().toString();



                            Query query = mDatabase.orderByKey().limitToLast(1);

                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                                        String s = postSnapshot.getKey().toString();
                                        DatabaseReference database = FirebaseDatabase.getInstance().getReference().child("EvenementUsers");
                                        database.child(s).child("userCreatoer").setValue(current_uid);
                                        testEventID = s;

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which){
                                        case DialogInterface.BUTTON_POSITIVE:

                                            Intent intent = new Intent(EventActivity.this, MultipleImage.class);

                                            intent.putExtra("evenetKey",testEventID);
                                            startActivity(intent);

                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            dialog.dismiss();
                                            Toast.makeText(EventActivity.this, "Evenement bien inscrit", Toast.LENGTH_LONG).show();
                                            Intent intent1 = new Intent(EventActivity.this, DashbordActivity.class);
                                            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent1);
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(EventActivity.this);
                            builder.setMessage("Vous voulez ajouter des photos ?").setPositiveButton("Oui", dialogClickListener)
                                    .setNegativeButton("Non", dialogClickListener).show();





                        }else{
                            Toast.makeText(getApplicationContext(), "non ", Toast.LENGTH_LONG).show();


                        }
                    }
                });










            }
            });


    }





    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            if(places.getStatus().isSuccess()){
                final Place place = places.get(0);
                CharSequence attributions = places.getAttributions();
                LatLng queriedLocation = place.getLatLng();
                Log.v("lat",""+queriedLocation.latitude);
                Log.v("lang",""+queriedLocation.longitude);

                lat = String.valueOf(queriedLocation.latitude);
                lang = String.valueOf(queriedLocation.longitude);
                keyIsWebSite = String.valueOf(place.getWebsiteUri());
                ifLatNoUpdate = lat;
                ifLangNoUpdate = lang;
                ifKeyNoUpdate = keyIsWebSite;
                
            }




        }
    };
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {
        if (v == dateDebut) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            String test="Date de début ";

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dateDebut.setText("Date de début " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == TimeDeb) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            TimeDeb.setText("Heure de début " + hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }

        if (v == dateFin) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            String test="Date de début ";

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dateFin.setText("Date du fin " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == TimeFin) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            TimeFin.setText("Heure du fin " + hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }


    }

    public void verifierDate(Date date, Date date1)
    {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
           Date ds = sdf.parse(date.toString());
           Date df = sdf.parse(date1.toString());

           if(ds.equals(df)){
               Toast.makeText(getApplicationContext(), "égaux", Toast.LENGTH_LONG).show();
           }

           if(ds.after(df)){
               Toast.makeText(getApplicationContext(), "ds super", Toast.LENGTH_LONG).show();

           }

            if(ds.before(df)){
                Toast.makeText(getApplicationContext(), "df super", Toast.LENGTH_LONG).show();

            }


        }catch (ParseException ex){

            Log.i("tag","parse exception");
        }
    }

    public void showChangeLangDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        dialogBuilder.setTitle("Prix");
        dialogBuilder.setMessage("prix de billet ou reservation");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                getPrixFormEvent = edt.getText().toString();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private String passKey(String key)
    {
        return key;
    }


    public void InsertData(final String title, final String lieu, final String dateDeb, final String dateFin, final String type){

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {

                String TitleHolder = title;
                String LieruHolder = lieu ;
                String DateDHolder = dateDeb ;
                String DateFHolder = dateFin ;
                String TypeeHolder = type ;

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("title", TitleHolder));
                nameValuePairs.add(new BasicNameValuePair("lieu", LieruHolder));
                nameValuePairs.add(new BasicNameValuePair("dateDeb", DateDHolder));
                nameValuePairs.add(new BasicNameValuePair("dateFin", DateFHolder));
                nameValuePairs.add(new BasicNameValuePair("type", TypeeHolder));

                try {
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(ServerURL);

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse httpResponse = httpClient.execute(httpPost);

                    HttpEntity httpEntity = httpResponse.getEntity();


                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
                return "Data Inserted Successfully";
            }

            @Override
            protected void onPostExecute(String result) {

                super.onPostExecute(result);

//                Toast.makeText(InscritActivity.this, "Data Submit Successfully", Toast.LENGTH_LONG).show();

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();


        sendPostReqAsyncTask.execute(title, lieu, dateDeb, dateFin, type);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
}
