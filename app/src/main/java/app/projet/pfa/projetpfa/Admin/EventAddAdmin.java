package app.projet.pfa.projetpfa.Admin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import app.projet.pfa.projetpfa.DashbordActivity;
import app.projet.pfa.projetpfa.Evenement;
import app.projet.pfa.projetpfa.EventActivity;
import app.projet.pfa.projetpfa.MultipleImage;
import app.projet.pfa.projetpfa.PlaceArrayAdapter;
import app.projet.pfa.projetpfa.R;
import app.projet.pfa.projetpfa.RequeteFragment;
import app.projet.pfa.projetpfa.viewEventActivity;


public class EventAddAdmin extends AppCompatActivity {


    private View mainView;
    private String current_id_user;
    static String key;
    private RecyclerView recyclerViewFriend;
    private DatabaseReference databaseReference, databaseReferenceUsers, databaseReferenceKey;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReferenceEvenet;


    static String clubkey;
    FloatingActionButton floatingActionButton;
    FusedLocationProviderClient mFusedLocationClient;
    private static double latiCurrent, langiCurrent;
    MaterialSearchView materialSearchView;
    String lat;
    String lang;
    double castLat;
    double castLang;
    float[] result = new float[5];
    Location location;


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_add_admin);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(EventAddAdmin.this);


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(EventAddAdmin.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            latiCurrent = location.getLatitude();
                            langiCurrent = location.getLongitude();

                            Log.d("latiCurrent", String.valueOf(latiCurrent));
                            Log.d("langiCurrent", String.valueOf(langiCurrent));


                        }
                    }
                });


        recyclerViewFriend = (RecyclerView)findViewById(R.id.event_list);
        firebaseAuth = FirebaseAuth.getInstance();
        current_id_user = firebaseAuth.getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceKey = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReference.keepSynced(true);
        databaseReferenceKey.keepSynced(true);
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReferenceUsers.keepSynced(true);
        recyclerViewFriend.setHasFixedSize(true);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(EventAddAdmin.this));

    }



        @Override
        public void onStart() {
            super.onStart();
            FirebaseRecyclerAdapter<Evenement, UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, UsersHolder>(

                    Evenement.class,
                    R.layout.event_single_layout,
                    EventAddAdmin.UsersHolder.class,
                    databaseReference
            ) {
                @Override
                protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                    viewHolder.setTitle(model.getTitle());
                    viewHolder.setLieu(model.getLieu());
                    viewHolder.setDate(model.getDateDeb());

                    String cat = model.getCategorie().toString();



                    if(cat.equals("Sport")){
                        viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, EventAddAdmin.this);


                    }else if(cat.equals("Musique")){
                        viewHolder.setUserImage(R.drawable.ic_audiotrack_light, EventAddAdmin.this);


                    }else if(cat.equals("Rencontre")){
                        viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, EventAddAdmin.this);


                    }else if(cat.equals("Autre")){

                        viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, EventAddAdmin.this);


                    }


                    final  String event_id = getRef(position).getKey();

                    viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent profileIntent = new Intent(EventAddAdmin.this, viewEventActivity.class);
                            profileIntent.putExtra("event_id", event_id);
                            startActivity(profileIntent);
                        }
                    });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


                }



            };

            recyclerViewFriend.setAdapter(firebaseRecyclerAdapter);
        }

        public static class UsersHolder extends RecyclerView.ViewHolder {

            View mView;

            public UsersHolder(View itemView) {
                super(itemView);

                mView = itemView;
            }

            public void setLieu(String date) {

                TextView textView = (TextView) mView.findViewById(R.id.textView3);
                textView.setText(date);
            }

            public void setTitle(String name) {

                TextView textView = (TextView) mView.findViewById(R.id.textView2);
                textView.setText(name);
            }

            public void setDate(String name) {

                TextView textView = (TextView) mView.findViewById(R.id.textView4);
                textView.setText(name);
            }

            public void setUserImage(int thumb_image, Context ctx) {

                ImageView userImageView = (ImageView) mView.findViewById(R.id.imageView4);

                userImageView.setImageResource(thumb_image);



            }



        }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
//        getMenuInflater().inflate(R.menu.menu_serach_users, menu);
//
//        MenuItem item = menu.findItem(R.id.search);
//        materialSearchView.setMenuItem(item);
//
//        return true;
//    }


    }



