package app.projet.pfa.projetpfa;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.googlecode.mp4parser.authoring.Track;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import id.zelory.compressor.Compressor;

import static app.projet.pfa.projetpfa.EventActivity.ifLangNoUpdate;
import static app.projet.pfa.projetpfa.EventActivity.ifLieuNoUpdate;
import static app.projet.pfa.projetpfa.GetLocationAdress.MapsActivity.getDefaults;
import static app.projet.pfa.projetpfa.viewEventActivity.passingDataLat;


public class updateEvent extends AppCompatActivity implements   GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,  View.OnClickListener {

    static String tes;
    private DatabaseReference mDatabase,databaseReferenceEvenet;
    Button btnDatePicker, btnTimePicker,buttonVider,buttonRemplir;
    EditText txtDate, txtTime,input_title;
    private StorageReference mStorageRef;
    RadioGroup rgType;
    private FirebaseUser firebaseUser;
    static String radioData, uid, getSpinerItem ;
    private static final int GALLERY_PIK1 = 1;
    MultiAutoCompleteTextView multiAutoCompleteTextView;
    private FirebaseAuth auth;
    List<Evenement> evenements;
    Button btnSaveData;
    TextView dateDebut,dateFin,TimeDeb,TimeFin;
    Spinner spinner;
    TextInputLayout textInputLayoutDateDeb;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private static final String TAG = "EventActivity";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private AutoCompleteTextView mAutocompleteTextView;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    String radioBtn;
    private static String lang;
    private static String lat;
    private static String keyIsWebSite;
    static  String maTest;
    String getEventId;
    private static String getLang, getLat, getKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_event);




        //get event id from intent

        getEventId = getIntent().getStringExtra("evnt_id");

        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement").child(getEventId);



        getDataEvent();

        mAutocompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewU);
        mAutocompleteTextView.setThreshold(3);
        input_title = (EditText)findViewById(R.id.input_titleU);
        dateDebut= (TextView)findViewById(R.id.in_datedU);
        TimeDeb= (TextView)findViewById(R.id.btn_timeU);
        dateFin=(TextView) findViewById(R.id.dateFinU);
        TimeFin=(TextView) findViewById(R.id.TimeFinU);
        btnSaveData = (Button) findViewById(R.id.btnSaveDataU);
        rgType = (RadioGroup)findViewById(R.id.rgTypeU);
        multiAutoCompleteTextView = (MultiAutoCompleteTextView)findViewById(R.id.multiAutoCompleteTextViewU);
         spinner = (Spinner) findViewById(R.id.spinnerU);
        buttonVider = (Button)findViewById(R.id.btnVider);
        buttonRemplir = (Button)findViewById(R.id.btnRemplir);


        mStorageRef = FirebaseStorage.getInstance().getReference();
        dateDebut.setOnClickListener(this);
        TimeDeb.setOnClickListener(this);
        dateFin.setOnClickListener(this);
        TimeFin.setOnClickListener(this);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();



        String current_uid = firebaseUser.getUid();
        auth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        List<String> categories = new ArrayList<String>();
        categories.add("Musique");
        categories.add("Rencontre");
        categories.add("Sport");
        categories.add("Culture");
        categories.add("Autre");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);
        spinner.setPrompt("Choisir la categorie ");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getSpinerItem = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // getSpinerItem = parent.getItemAtPosition(position).toString();


        uid= user.getUid();
        mGoogleApiClient = new GoogleApiClient.Builder(updateEvent.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);


        mDatabase = FirebaseDatabase.getInstance().getReference("Evenement");




        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioButton:
                        radioBtn ="Gratuit";
                        break;
                    case R.id.radioButton2:
                        radioBtn ="Payant";
                        break;

                }
            }
        });



        buttonRemplir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataEvent();
            }
        });

        buttonVider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viderData();
            }
        });







        Toast.makeText(updateEvent.this, passingDataLat, Toast.LENGTH_LONG).show();

        btnSaveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                updateFrom();
//                Intent intent = new Intent(updateEvent.this, RequeteFragment.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);


            }
        });



    }



    private boolean updateData (String id ,String title, String lieu, String dateDeb, String heureDeb,
                                String dateFin, String heureFin, String type, String description,
                                String userCr, String categorie, String image, String thumb_image,
                                String key, String lat, String lang){

        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("Evenement").child(id);

        Evenement evenement = new Evenement(title,  lieu,  dateDeb,  heureDeb,
                 dateFin,  heureFin,  type,  description,
                 userCr,  categorie,  image,  thumb_image,
                 key,  lat,  lang);


        dR.setValue(evenement);


                return true;
    }

    private void viderData() {

        String str="";
        String dateDebDefaultValue="Date de début";
        String datefinDefaultValue="Date du fin";
        String heurDebDefaultValue="Heure de début";
        String heurFinDefaultValue="Heure du fin";


        input_title.setText(str);
        mAutocompleteTextView.setText(str);
        dateDebut.setText(dateDebDefaultValue);
        TimeDeb.setText(heurDebDefaultValue);
        dateFin.setText(datefinDefaultValue);
        TimeFin.setText(heurFinDefaultValue);
        multiAutoCompleteTextView.setText(str);
        spinner.setSelection(0);
    }

    private void getDataEvent() {

        databaseReferenceEvenet.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


               getLat = dataSnapshot.child("lat").getValue().toString();
               getLang = dataSnapshot.child("lang").getValue().toString();
               getKey = dataSnapshot.child("key").getValue().toString();
                maTest = dataSnapshot.child("lieu").getValue().toString();

             Evenement evenement = dataSnapshot.getValue(Evenement.class);

             String ttitle = evenement.getTitle();
             String lieu = evenement.getLieu();
             String dateDeb = evenement.getDateDeb();
             String datefin = evenement.getDateFin();
             String herdeb = evenement.getHeureDeb();
             String herfin = evenement.getHeureFin();
             String type = evenement.getType();
             String desc = evenement.getDescription();
             String cat = evenement.getCategorie();
             int indexSpinner = 0;

             if(cat.equals("Musique"))
                 indexSpinner = 0;
                if(cat.equals("Rencontre"))
                    indexSpinner = 1;
                if(cat.equals("Sport"))
                    indexSpinner = 2;
                if(cat.equals("Culture"))
                    indexSpinner = 3;
                if(cat.equals("Autre"))
                    indexSpinner = 4;



                input_title.setText(ttitle);
                mAutocompleteTextView.setText(lieu);
                dateDebut.setText(dateDeb);
                TimeDeb.setText(herdeb);
                dateFin.setText(datefin);
                TimeFin.setText(herfin);
                multiAutoCompleteTextView.setText(desc);
                spinner.setSelection(indexSpinner);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            if(places.getStatus().isSuccess()){
                final Place place = places.get(0);
                CharSequence attributions = places.getAttributions();
                LatLng queriedLocation = place.getLatLng();
                keyIsWebSite = String.valueOf(place.getWebsiteUri());
                Log.v("latup",""+queriedLocation.latitude);
                Log.v("langup",""+queriedLocation.longitude);
                Log.v("keyup",""+keyIsWebSite);

                lat = String.valueOf(queriedLocation.latitude);
                lang = String.valueOf(queriedLocation.longitude);


            }




        }
    };
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {
        if (v == dateDebut) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            String test="Date de début ";

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dateDebut.setText("Date de début " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == TimeDeb) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            TimeDeb.setText("Heure de début " + hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }

        if (v == dateFin) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            String test="Date de début ";

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dateFin.setText("Date du fin " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == TimeFin) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            TimeFin.setText("Heure du fin " + hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }


    }

    public void verifierDate(Date date, Date date1)
    {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date ds = sdf.parse(date.toString());
            Date df = sdf.parse(date1.toString());

            if(ds.equals(df)){
                Toast.makeText(getApplicationContext(), "égaux", Toast.LENGTH_LONG).show();
            }

            if(ds.after(df)){
                Toast.makeText(getApplicationContext(), "ds super", Toast.LENGTH_LONG).show();

            }

            if(ds.before(df)){
                Toast.makeText(getApplicationContext(), "df super", Toast.LENGTH_LONG).show();

            }


        }catch (ParseException ex){

            Log.i("tag","parse exception");
        }
    }


    private void updateFrom() {

        String mTitle = input_title.getText().toString().trim();
        String mLieu  = mAutocompleteTextView.getText().toString() ;
        String mDateDeb = dateDebut.getText().toString();
        String mHeureDef = TimeFin.getText().toString();
        String mDateFin = dateFin.getText().toString();
        String mHeureDeb = TimeDeb.getText().toString();
        String mType = radioBtn;
        String mDesciption = multiAutoCompleteTextView.getText().toString();
        String cat = getSpinerItem;


//        Toast.makeText(updateEvent.this,mLieu,Toast.LENGTH_LONG).show();
//        Toast.makeText(updateEvent.this,maTest,Toast.LENGTH_LONG).show();
//        Toast.makeText(updateEvent.this,getKey,Toast.LENGTH_LONG).show();
//        Toast.makeText(updateEvent.this,getLat,Toast.LENGTH_LONG).show();
//        Toast.makeText(updateEvent.this,getLang,Toast.LENGTH_LONG).show();


        if(mLieu.equals(maTest.trim())) {
            if( updateData(getEventId, mTitle, mLieu, mDateDeb, mHeureDeb,
                    mDateFin, mHeureDef,  mType, mDesciption, uid, cat,
                    "image", "thumb_image", getKey, getLat, getLang))

                Toast.makeText(updateEvent.this, "Evenement bien modifie", Toast.LENGTH_LONG).show();



            else
                Toast.makeText(updateEvent.this,"non", Toast.LENGTH_LONG).show();

        }
        else{

            if(keyIsWebSite.isEmpty() || keyIsWebSite== null)
            {
                keyIsWebSite ="key";
            }
            if( updateData(getEventId, mTitle, mLieu, mDateDeb, mHeureDeb,
                    mDateFin, mHeureDef,  mType, mDesciption, uid, cat,
                    "image", "thumb_image", keyIsWebSite, lat, lang))

                Toast.makeText(updateEvent.this, "Evenement bien modifie", Toast.LENGTH_LONG).show();



            else
                Toast.makeText(updateEvent.this,"non", Toast.LENGTH_LONG).show();

        }


    }

}
