package app.projet.pfa.projetpfa;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static app.projet.pfa.projetpfa.EventActivity.tes;
import static app.projet.pfa.projetpfa.PrincipaleActivity.string;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequeteFragment extends Fragment {

    private View mainView;
    private String current_id_user;
    static  String key;
    private RecyclerView recyclerViewFriend;
    private DatabaseReference databaseReference, databaseReferenceUsers,databaseReferenceKey;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReferenceEvenet;


    static  String clubkey;
    FloatingActionButton floatingActionButton;
    FusedLocationProviderClient mFusedLocationClient;
    private static double latiCurrent, langiCurrent;
    MaterialSearchView materialSearchView;
    String lat;
    String lang;
    double castLat;
    double castLang;
    float[] result=new float[5];
    Location location;


    public RequeteFragment() {
        // Required empty public constructor
    }


    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mainView = inflater.inflate(R.layout.fragment_requete, container, false);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            latiCurrent = location.getLatitude();
                            langiCurrent = location.getLongitude();

                            Log.d("latiCurrent", String.valueOf(latiCurrent));
                            Log.d("langiCurrent", String.valueOf(langiCurrent));


                        }
                    }
                });





        recyclerViewFriend = (RecyclerView) mainView.findViewById(R.id.event_list);
        firebaseAuth = FirebaseAuth.getInstance();
        current_id_user = firebaseAuth.getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceEvenet = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReferenceKey = FirebaseDatabase.getInstance().getReference().child("Evenement");
        databaseReference.keepSynced(true);
        databaseReferenceKey.keepSynced(true);
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        databaseReferenceUsers.keepSynced(true);
        recyclerViewFriend.setHasFixedSize(true);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(getContext()));



        if(string != null)
        {
            if(string.equals("oui"))
            {
                //  Toast.makeText(getContext(),"Yes"+string, Toast.LENGTH_LONG).show();

            }else
            {
                //Toast.makeText(getContext(),"No"+string, Toast.LENGTH_LONG).show();

            }
        }


        //  evntLatLan();



        return mainView;

    }





//    private void evntLatLan()
//    {
//
//
//        databaseReferenceEvenet.child("-LA_ZAxEHSP5jSCch6GD").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                String lat = dataSnapshot.child("lat").getValue().toString();
//                String lang = dataSnapshot.child("lang").getValue().toString();
//
//                double castLat = Double.parseDouble(lat);
//                double castLang = Double.parseDouble(lat);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//        latLngS = new LatLng(latiCurrent, langiCurrent);
//        latLngE = new LatLng(castLat, castLang);

//        Location locationA = new Location("");
//        locationA.setLatitude(latiCurrent);
//        locationA.setLongitude(langiCurrent);
//
//        Location locationB = new Location("");
//        locationB.setLatitude(castLat);
//        locationB.setLongitude(castLang);
//
//
//       double distance = 0;
//
//       distance = locationA.distanceTo(locationB);
//
//
//        Toast.makeText(getActivity(),"" +  distance, Toast.LENGTH_LONG).show();
//








    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Evenement, RequeteFragment.UsersHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Evenement, RequeteFragment.UsersHolder>(

                Evenement.class,
                R.layout.event_single_layout,
                RequeteFragment.UsersHolder.class,
                databaseReference
        ) {
            @Override
            protected void populateViewHolder(final UsersHolder viewHolder, Evenement model, int position) {


                viewHolder.setTitle(model.getTitle());
                viewHolder.setLieu(model.getLieu());
                viewHolder.setDate(model.getDateDeb());

                String cat = model.getCategorie().toString();



                if(cat.equals("Sport")){
                    viewHolder.setUserImage(R.drawable.ic_directions_bike_black_24dp, getActivity().getApplicationContext());


                }else if(cat.equals("Musique")){
                    viewHolder.setUserImage(R.drawable.ic_audiotrack_light, getActivity().getApplicationContext());


                }else if(cat.equals("Rencontre")){
                    viewHolder.setUserImage(R.drawable.ic_wc_black_24dp, getActivity().getApplicationContext());


                }else if(cat.equals("Autre")){

                    viewHolder.setUserImage(R.drawable.ic_format_align_center_black_24dp, getActivity().getApplicationContext());


                }


                final  String event_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(getActivity(), viewEventActivity.class);
                        profileIntent.putExtra("event_id", event_id);
                        startActivity(profileIntent);
                    }
                });
//                databaseReference.child(list_users_id_firend).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String nom = dataSnapshot.child("title").getValue().toString();
//                        String imageThumb = dataSnapshot.child("lieu").getValue().toString();
//                        String date = dataSnapshot.child("dateDeb").getValue().toString();
//
//
//                        viewHolder.setTitle(nom);
//                        viewHolder.setLieu(imageThumb);
//                        viewHolder.setDate(date);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }) ;


            }



        };

        recyclerViewFriend.setAdapter(firebaseRecyclerAdapter);
    }

    public static class UsersHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setLieu(String date) {

            TextView textView = (TextView) mView.findViewById(R.id.textView3);
            textView.setText(date);
        }

        public void setTitle(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.textView2);
            textView.setText(name);
        }

        public void setDate(String name) {

            TextView textView = (TextView) mView.findViewById(R.id.textView4);
            textView.setText(name);
        }

        public void setUserImage(int thumb_image, Context ctx) {

            ImageView userImageView = (ImageView) mView.findViewById(R.id.imageView4);
            userImageView.setImageResource(thumb_image);


        }



    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
//        getMenuInflater().inflate(R.menu.menu_serach_users, menu);
//
//        MenuItem item = menu.findItem(R.id.search);
//        materialSearchView.setMenuItem(item);
//
//        return true;
//    }


}



