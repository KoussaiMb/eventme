package app.projet.pfa.projetpfa;

import java.util.Date;

public class Evenement {

    private String id;
    private String title;
    private String lieu;
    private String DateDeb;
    private String HeureDeb;
    private String DateFin;
    private String HeureFin;
    private String type;
    private String description;
    private String userCr;
    private String categorie;
    private String image;
    private String thumb_image;
    private String image1;
    private String thumb_image1;
    private String image2;
    private String thumb_image2;
    private String key;
    private String lat;
    private String lang;


    public Evenement() {
    }

    public Evenement(String title, String lieu, String dateDeb) {
        this.title = title;
        this.lieu = lieu;
        DateDeb = dateDeb;
    }

    public Evenement(String id, String title, String lieu, String dateDeb, String heureDeb, String dateFin, String heureFin, String type, String userCr, String categorie, String image, String thumb_image, String image1, String thumb_image1, String image2, String thumb_image2, String key, String lat, String lang) {
        this.id = id;
        this.title = title;
        this.lieu = lieu;
        DateDeb = dateDeb;
        HeureDeb = heureDeb;
        DateFin = dateFin;
        HeureFin = heureFin;
        this.type = type;
        this.userCr = userCr;
        this.categorie = categorie;
        this.image = image;
        this.thumb_image = thumb_image;
        this.image1 = image1;
        this.thumb_image1 = thumb_image1;
        this.image2 = image2;
        this.thumb_image2 = thumb_image2;
        this.key = key;
        this.lat = lat;
        this.lang = lang;
    }


    public Evenement(String id, String title, String lieu, String dateDeb, String heureDeb, String dateFin, String heureFin, String type, String description, String userCr, String categorie, String image, String thumb_image, String image1, String thumb_image1, String image2, String thumb_image2, String key, String lat, String lang) {
        this.id = id;
        this.title = title;
        this.lieu = lieu;
        DateDeb = dateDeb;
        HeureDeb = heureDeb;
        DateFin = dateFin;
        HeureFin = heureFin;
        this.type = type;
        this.description = description;
        this.userCr = userCr;
        this.categorie = categorie;
        this.image = image;
        this.thumb_image = thumb_image;
        this.image1 = image1;
        this.thumb_image1 = thumb_image1;
        this.image2 = image2;
        this.thumb_image2 = thumb_image2;
        this.key = key;
        this.lat = lat;
        this.lang = lang;
    }

    public Evenement(String title, String lieu, String dateDeb, String heureDeb, String dateFin, String heureFin, String type, String description, String userCr, String categorie, String image, String thumb_image, String key, String lat, String lang) {
        this.title = title;
        this.lieu = lieu;
        DateDeb = dateDeb;
        HeureDeb = heureDeb;
        DateFin = dateFin;
        HeureFin = heureFin;
        this.type = type;
        this.description = description;
        this.userCr = userCr;
        this.categorie = categorie;
        this.image = image;
        this.thumb_image = thumb_image;
        this.key = key;
        this.lat = lat;
        this.lang = lang;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getThumb_image1() {
        return thumb_image1;
    }

    public void setThumb_image1(String thumb_image1) {
        this.thumb_image1 = thumb_image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getThumb_image2() {
        return thumb_image2;
    }

    public void setThumb_image2(String thumb_image2) {
        this.thumb_image2 = thumb_image2;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getUserCr() {
        return userCr;
    }

    public void setUserCr(String userCr) {
        this.userCr = userCr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getDateDeb() {
        return DateDeb;
    }

    public void setDateDeb(String dateDeb) {
        DateDeb = dateDeb;
    }

    public String getHeureDeb() {
        return HeureDeb;
    }

    public void setHeureDeb(String heureDeb) {
        HeureDeb = heureDeb;
    }

    public String getDateFin() {
        return DateFin;
    }

    public void setDateFin(String dateFin) {
        DateFin = dateFin;
    }

    public String getHeureFin() {
        return HeureFin;
    }

    public void setHeureFin(String heureFin) {
        HeureFin = heureFin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Evenement{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", lieu='" + lieu + '\'' +
                ", DateDeb='" + DateDeb + '\'' +
                ", HeureDeb='" + HeureDeb + '\'' +
                ", DateFin='" + DateFin + '\'' +
                ", HeureFin='" + HeureFin + '\'' +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", userCr='" + userCr + '\'' +
                ", categorie='" + categorie + '\'' +
                ", image='" + image + '\'' +
                ", thumb_image='" + thumb_image + '\'' +
                ", image1='" + image1 + '\'' +
                ", thumb_image1='" + thumb_image1 + '\'' +
                ", image2='" + image2 + '\'' +
                ", thumb_image2='" + thumb_image2 + '\'' +
                '}';
    }
}
