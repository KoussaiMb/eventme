package app.projet.pfa.projetpfa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button button,button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.inscr);
        button1 = (Button)findViewById(R.id.conn);
    }

    public void inscr(View view) {

        startActivity(new Intent(MainActivity.this,InscritActivity.class));
    }

    public void conn(View view) {
        startActivity(new Intent(MainActivity.this,SignInActivity.class));

    }
}
