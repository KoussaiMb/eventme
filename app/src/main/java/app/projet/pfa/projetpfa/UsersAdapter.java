package app.projet.pfa.projetpfa;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder> {

    private List<Users> moviesList;

    @Override
    public UsersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_user_test, parent, false);

        return new UsersAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UsersAdapter.MyViewHolder holder, int position) {
        Users movie = moviesList.get(position);
        holder.textView22.setText(movie.getNom());
        holder.textView33.setText(movie.getPays());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView22, textView33, textView44 ;

        public MyViewHolder(View view) {
            super(view);
            textView22 = (TextView) view.findViewById(R.id.user_single_name);
            textView33 = (TextView) view.findViewById(R.id.user_single_status);
        }
    }

    public UsersAdapter(List<Users> moviesList)
    {
        this.moviesList = moviesList;
    }

}

